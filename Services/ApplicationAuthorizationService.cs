﻿using AngularLibrary.JWT.Helpers;
using AngularLibrary.JWT.TokenGeneration;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.AuthorizationRequestModels;
using AngularLibrary.ViewModels.AuthorizationViewModel;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AngularLibrary.Services
{
    public class ApplicationAuthorizationService : IApplicationAuthorizationService
    {
        private UserManager<IdentityUser> _userManager;
        private IJwtFactory _jwtFactory;

        public ApplicationAuthorizationService(UserManager<IdentityUser> userManager, IJwtFactory jwtFactory)
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string pass, string role)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(pass))
            {
                return null;
            }

            var userToVerify = await _userManager.FindByNameAsync(userName);

            if (userToVerify == null)
            {
                return null;
            }

            if (!await _userManager.CheckPasswordAsync(userToVerify, pass))
            {
                return null;
            }

            var result = _jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id, role);

            return result;
        }

        public async Task<LoginResponseModel> Login(AuthorizationRequestModel userViewModel)
        {
            IdentityUser user = await _userManager.FindByNameAsync(userViewModel.Name);

            if (user == null)
            {
                return null;
            }

            var role = await _userManager.GetRolesAsync(user);
            string permissionToFrontend = null;
            ClaimsIdentity identity = null;

            if (role.Contains(JWTStrings.AppUserRoleName))
            {
                permissionToFrontend = JWTStrings.IdentifierRoleUser;
                identity = await GetClaimsIdentity(userViewModel.Name, userViewModel.Password, JWTStrings.IdentifierRoleUser);
            }

            if (role.Contains(JWTStrings.AppAdminRoleName))
            {
                permissionToFrontend = JWTStrings.IdentifierRoleAdmin;
                identity = await GetClaimsIdentity(userViewModel.Name, userViewModel.Password, JWTStrings.IdentifierRoleAdmin);
            }

            if (identity == null)
            {
                return null;
            }

            var jwt = _jwtFactory.GenerateEncodedToken(user.UserName, identity);

            var result = new LoginResponseModel { AuthorizationToken = jwt, Permission = permissionToFrontend };

            return result;
        }

        public async Task<bool> AddAdmin(AuthorizationRequestModel userViewModel)
        {
            var user = new IdentityUser()
            {
                UserName = userViewModel.Name,
                Email = userViewModel.EMail
            };

            var result = await _userManager.CreateAsync(user, userViewModel.Password);

            if (!result.Succeeded)
            {
                return false;
            }

            await _userManager.AddToRoleAsync(user, JWTStrings.AppAdminRoleName);

            return true;
        }

        public async Task<bool> Register(AuthorizationRequestModel userViewModel)
        {
            var user = new IdentityUser() { UserName = userViewModel.Name, Email = userViewModel.EMail };
            var result = await _userManager.CreateAsync(user, userViewModel.Password);

            if (!result.Succeeded)
            {
                return false;
            }

            await _userManager.AddToRoleAsync(user, JWTStrings.AppUserRoleName);

            return true;
        }
    }
}
