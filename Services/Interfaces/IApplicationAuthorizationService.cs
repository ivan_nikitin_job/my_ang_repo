﻿using AngularLibrary.ViewModels.AuthorizationRequestModels;
using AngularLibrary.ViewModels.AuthorizationViewModel;
using System.Threading.Tasks;

namespace AngularLibrary.Services.Interfaces
{
    public interface IApplicationAuthorizationService
    {
        Task<bool> AddAdmin(AuthorizationRequestModel userViewModel);
        Task<bool> Register(AuthorizationRequestModel userViewModel);
        Task<LoginResponseModel> Login(AuthorizationRequestModel userViewModel);
    }
}