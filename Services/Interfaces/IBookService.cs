﻿using AngularLibrary.ViewModels.BookViewModels;
using System.Threading.Tasks;

namespace AngularLibrary.Services.Interfaces
{
    public interface IBookService
    {
        void CreateBook(CreateBookRequestModel bookViewModel);
        void UpdateBook(UpdateBookRequestModel bookViewModel);
        void DeleteBook(long id);
        Task<GetSimpleBookViewModel> GetBookListModelByPublicationHouseID(long id);
        Task<GetSimpleBookViewModel> GetSimpleBookList();
        Task<GetComplexBookViewModel> GetComplexBookList();
    }
}