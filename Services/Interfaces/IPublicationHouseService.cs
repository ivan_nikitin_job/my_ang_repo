﻿using AngularLibrary.ViewModels.PublicationHouseViewModels;
using System.Threading.Tasks;

namespace AngularLibrary.Services.Interfaces
{
    public interface IPublicationHouseService
    {
        void CreatePublicationHouse(CreatePublicationHouseRequestModel publicationHouseViewModel);
        void UpdatePublicationHouse(UpdatePublicationHouseRequestModel publicationHouseViewModel);
        void DeletePublicationHouse(long id);
        Task<GetSimplePublicationHouseViewModel> GetPublicationHouseListModelByBookID(long id);
        Task<GetComplexPublicationHouseViewModel> GetComplexPublicationHouseList();
        Task<GetSimplePublicationHouseViewModel> GetSimplePublicationHouseList();
    }
}