﻿using AngularLibrary.ViewModels.BrochureViewModels;
using System.Threading.Tasks;

namespace AngularLibrary.Services.Interfaces
{
    public interface IBrochureService
    {
        void CreateBrochure(CreateBrochureRequestModel brochure);
        void UpdateBrochure(UpdateBrochureRequestModel brochure);
        void DeleteBrochure(long id);
        Task<GetBrochureViewModel> GetBrochureList();
    }
}