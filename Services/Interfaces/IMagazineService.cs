﻿using AngularLibrary.ViewModels.MagazineViewModels;
using System.Threading.Tasks;

namespace AngularLibrary.Services.Interfaces
{
    public interface IMagazineService
    {
        void CreateMagazine(CreateMagazineRequestModel magazine);
        void UpdateMagazine(UpdateMagazineRequestModel magazine);
        void DeleteMagazine(long id);
        Task<GetMagazineViewModel> GetMagazineList();
    }
}