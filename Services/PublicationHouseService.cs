﻿using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.Entities;
using AngularLibrary.Mapping.PublicationHouseMapping;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.PublicationHouseViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLibrary.Services
{
    public class PublicationHouseService : IPublicationHouseService
    {
        private IPublicationHouseBusinessLogic _publicationHouseBLL;
        private IBookPublicationHouseBusinessLogic _bookPublicationHouseBLL;
        private IBookBuisnessLogic _booksBLL;

        public PublicationHouseService(IPublicationHouseBusinessLogic publicationHouseBusinessLogic,
                                        IBookPublicationHouseBusinessLogic bookPublicationHouseBusinessLogic,
                                        IBookBuisnessLogic bookBuisnessLogic)
        {
            _publicationHouseBLL = publicationHouseBusinessLogic;
            _bookPublicationHouseBLL = bookPublicationHouseBusinessLogic;
            _booksBLL = bookBuisnessLogic;
        }

        public async Task<GetComplexPublicationHouseViewModel> GetComplexPublicationHouseList()
        {
            IEnumerable<Book> bookList = await _booksBLL.GetBookList();
            IEnumerable<PublicationHouse> publicationHouseList = await _publicationHouseBLL.GetPublicationHouseList();
            IEnumerable<BookPublicationHouse> bookPublicationHouseList = _bookPublicationHouseBLL.GetRelations();

            var result = PublicationHouseMappingProvider.MapGetComplexPublicationHouseViewModel(bookList, publicationHouseList, bookPublicationHouseList);

            return result;
        }

        public async Task<GetSimplePublicationHouseViewModel> GetSimplePublicationHouseList()
        {
            IEnumerable<PublicationHouse> publicationHouseList = await _publicationHouseBLL.GetPublicationHouseList();

            var result = PublicationHouseMappingProvider.MapPublicationHouseListToModel(publicationHouseList);

            return result;
        }

        public void CreatePublicationHouse(CreatePublicationHouseRequestModel publicationHouseRequestModel)
        {
            var publicationHouse = PublicationHouseMappingProvider.MapCreatePublicationHouseViewModelToPublicationHouse(publicationHouseRequestModel);

            _publicationHouseBLL.CreatePublicationHouse(publicationHouse, publicationHouseRequestModel.BookIdList);
        }

        public void DeletePublicationHouse(long id)
        {
            _publicationHouseBLL.DeletePublicationHouse(id);
        }

        public void UpdatePublicationHouse(UpdatePublicationHouseRequestModel publicationHouseRequestModel)
        {
            var publicationHouse = PublicationHouseMappingProvider.MapUpdatePublicationHouseRequestModelToPublicationHouse(publicationHouseRequestModel);

            _publicationHouseBLL.UpdatePublicationHouse(publicationHouse, publicationHouseRequestModel.BookIdList);
        }

        public async Task<GetSimplePublicationHouseViewModel> GetPublicationHouseListModelByBookID(long id)
        {
            IEnumerable<BookPublicationHouse> bookPublicationHouseList = _bookPublicationHouseBLL.GetRelationsByBookId(id);
            IEnumerable<PublicationHouse> publicationHouseList = await _publicationHouseBLL.GetPublicationHouseList();

            var selectResult = bookPublicationHouseList.Select(i => i.PublicationHouseId);

            List<PublicationHouse> publicationHouseListResult = publicationHouseList.Where(b => selectResult.Contains(b.Id)).ToList();

            var result = PublicationHouseMappingProvider.MapPublicationHouseListToModel(publicationHouseListResult);

            return result;
        }
    }
}
