﻿using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.Entities;
using AngularLibrary.Mapping.MagazineMapping;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.MagazineViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.Services
{
    public class MagazineService : IMagazineService
    {
        private IMagazineBusinessLogic _magazineBusinessLogic;

        public MagazineService(IMagazineBusinessLogic magazineBusinessLogic)
        {
            _magazineBusinessLogic = magazineBusinessLogic;
        }

        public async Task<GetMagazineViewModel> GetMagazineList()
        {
            IEnumerable<Magazine> magazineList = await _magazineBusinessLogic.GetMagazineList();

            var result = MagazineMappingProvider.MapMagazineToMagazineGetMagazineViewModel(magazineList);

            return result;
        }

        public void CreateMagazine(CreateMagazineRequestModel magazineRequestModel)
        {
            var magazine = MagazineMappingProvider.MapCreateMagazineRequestModelToMagazine(magazineRequestModel);

            _magazineBusinessLogic.CreateMagazine(magazine);
        }

        public void UpdateMagazine(UpdateMagazineRequestModel magazineRequestModel)
        {
            var magazine = MagazineMappingProvider.MapUpdateMagazineRequestModelToMagazine(magazineRequestModel);

            _magazineBusinessLogic.UpdateMagazine(magazine);
        }

        public void DeleteMagazine(long id)
        {
            _magazineBusinessLogic.DeleteMagazine(id);
        }
    }
}
