﻿using AngularLibrary.BLL;
using AngularLibrary.Entities;
using AngularLibrary.ViewModels;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.Services
{
    public class PublicationService
    {
        private BookBusinessLogic _bookBLL;
        private BrochureBusinessLogic _brochureBLL;
        private MagazineBusinessLogic _magazineBLL;
        private IMapper _mapper;

        public PublicationService(IConfiguration configuration, IMapper mapper)
        {
            _bookBLL = new BookBusinessLogic(configuration);
            _brochureBLL = new BrochureBusinessLogic(configuration);
            _magazineBLL = new MagazineBusinessLogic(configuration);
            _mapper = mapper;
        }

        public async Task<PublicationViewModel> GetAll()
        {
            var getList = new List<Publication>();

            var books = await _bookBLL.Get();
            getList.AddRange(books);

            var brochures = await _brochureBLL.Get();
            getList.AddRange(brochures);

            var magazines = await _magazineBLL.Get();
            getList.AddRange(magazines);

            var outList = new PublicationViewModel();
            foreach (Publication pub in getList)
            {
                outList.PublicationList.Add(_mapper.Map<Publication, PublicationViewItem>(pub));
            }

            return outList;
        }
    }
}
