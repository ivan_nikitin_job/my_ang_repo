﻿using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.Entities;
using AngularLibrary.Mapping.BookMapping;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.BookViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLibrary.Services
{
    public class BookService : IBookService
    {
        private IBookBuisnessLogic _booksBLL;
        private IBookPublicationHouseBusinessLogic _relationBLL;
        private IPublicationHouseBusinessLogic _publicationHouseBLL;

        public BookService(IBookBuisnessLogic bookBLL, IBookPublicationHouseBusinessLogic relationsBLL, IPublicationHouseBusinessLogic publicationHouseBLL)
        {
            _booksBLL = bookBLL;
            _relationBLL = relationsBLL;
            _publicationHouseBLL = publicationHouseBLL;
        }

        public async Task<GetComplexBookViewModel> GetComplexBookList()
        {
            IEnumerable<Book> bookList = await _booksBLL.GetBookList();
            IEnumerable<PublicationHouse> publicationHouseList = await _publicationHouseBLL.GetPublicationHouseList();
            IEnumerable<BookPublicationHouse> bookPublicationHouseList = _relationBLL.GetRelations();

            var result = BookMapperProvider.MapGetComplexBookViewModel(bookList, publicationHouseList, bookPublicationHouseList);

            return result;
        }

        public async Task<GetSimpleBookViewModel> GetSimpleBookList()
        {
            IEnumerable<Book> bookList = await _booksBLL.GetBookList();

            var result = BookMapperProvider.MapBookListToSimpleBookViewModel(bookList);

            return result;
        }

        public void CreateBook(CreateBookRequestModel bookRequestModel)
        {
            var book = BookMapperProvider.MapCreateBookRequestModelToBook(bookRequestModel);

            _booksBLL.CreateBook(book, bookRequestModel.PublicationHouseIdList);
        }

        public void DeleteBook(long id)
        {
            _booksBLL.DeleteBook(id);
        }

        public void UpdateBook(UpdateBookRequestModel bookRequestModel)
        {
            var book = BookMapperProvider.MapUpdateBookRequestModelToBook(bookRequestModel);

            _booksBLL.UpdateBook(book, bookRequestModel.PublicationHouseIdList);
        }

        public async Task<GetSimpleBookViewModel> GetBookListModelByPublicationHouseID(long id)
        {
            IEnumerable<BookPublicationHouse> bookPublicationHouseList = _relationBLL.GetRelationsByPublicationHouseId(id);
            IEnumerable<Book> bookList = await _booksBLL.GetBookList();

            var selectResult = bookPublicationHouseList.Select(i => i.BookId);

            List<Book> bookListResult = bookList.Where(b => selectResult.Contains(b.Id)).ToList();

            var result = BookMapperProvider.MapBookListToSimpleBookViewModel(bookListResult);

            return result;
        }
    }
}
