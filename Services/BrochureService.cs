﻿using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.Entities;
using AngularLibrary.Mapping.BrochureMapping;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.BrochureViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.Services
{
    public class BrochureService : IBrochureService
    {
        private IBrochureBusinessLogic _brochureBusinessLogic;

        public BrochureService(IBrochureBusinessLogic brochureBusinessLogic)
        {
            _brochureBusinessLogic = brochureBusinessLogic;
        }

        public async Task<GetBrochureViewModel> GetBrochureList()
        {
            IEnumerable<Brochure> brochureList = await _brochureBusinessLogic.GetBrochureList();

            var result = BrochureMappingProvider.MapBrochureListToGetBrochureViewModel(brochureList);

            return result;
        }

        public void CreateBrochure(CreateBrochureRequestModel brochureRequestModel)
        {
            var brochure = BrochureMappingProvider.MapCreateBrochureRequestModelToBrochure(brochureRequestModel);

            _brochureBusinessLogic.CreateBrochure(brochure);
        }

        public void DeleteBrochure(long id)
        {
            _brochureBusinessLogic.DeleteBrochure(id);
        }

        public void UpdateBrochure(UpdateBrochureRequestModel brochureRequestModel)
        {
            var brochure = BrochureMappingProvider.MapUpdateBrochureRequestModelToBrochure(brochureRequestModel);

            _brochureBusinessLogic.UpdateBrochure(brochure);
        }
    }
}
