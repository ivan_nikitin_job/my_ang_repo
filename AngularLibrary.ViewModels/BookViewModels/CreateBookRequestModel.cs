﻿using System.Collections.Generic;

namespace AngularLibrary.ViewModels.BookViewModels
{
    public class CreateBookRequestModel
    {
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public int YearOfPublish { get; set; }

        public IEnumerable<long> PublicationHouseIdList { get; set; }

        public CreateBookRequestModel()
        {
            PublicationHouseIdList = new List<long>();
        }
    }
}
