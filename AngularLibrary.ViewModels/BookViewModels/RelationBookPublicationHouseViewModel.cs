﻿namespace AngularLibrary.ViewModels.BookViewModels
{
    public class RelationBookPublicationHouseViewModel
    {
        public long BookID { get; set; }
        public long PublicationHouseID { get; set; }
    }
}
