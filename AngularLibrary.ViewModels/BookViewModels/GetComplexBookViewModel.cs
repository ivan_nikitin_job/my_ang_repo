using System.Collections.Generic;

namespace AngularLibrary.ViewModels.BookViewModels
{
    public class GetComplexBookViewModel
    {
        public List<BookGetComplexBookViewModelItem> BookList { get; set; }

        public GetComplexBookViewModel()
        {
            BookList = new List<BookGetComplexBookViewModelItem>();
        }
    }

    public class BookGetComplexBookViewModelItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public int YearOfPublish { get; set; }
        public IEnumerable<PublicationHouseViewModelItem> PublicationHouseList { get; set; }
    }

    public class PublicationHouseViewModelItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
