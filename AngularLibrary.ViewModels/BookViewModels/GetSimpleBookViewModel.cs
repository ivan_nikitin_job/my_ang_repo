﻿using System.Collections.Generic;

namespace AngularLibrary.ViewModels.BookViewModels
{
    public class GetSimpleBookViewModel
    {
        public List<BookGetSimpleBookViewModelItem> BookList { get; set; }

        public GetSimpleBookViewModel()
        {
            BookList = new List<BookGetSimpleBookViewModelItem>();
        }
    }

    public class BookGetSimpleBookViewModelItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public int YearOfPublish { get; set; }
    }
}
