namespace AngularLibrary.ViewModels.AuthorizationViewModel
{
    public class LoginResponseModel
    {
        public string AuthorizationToken { get; set; }
        public string Permission { get; set; }
    }
}
