﻿namespace AngularLibrary.ViewModels.Enums
{
    public enum TypeOfCoverViewModel
    {
        Hard = 0,
        Soft = 1
    }
}
