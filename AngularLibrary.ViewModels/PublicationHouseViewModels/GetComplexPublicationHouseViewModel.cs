using System.Collections.Generic;

namespace AngularLibrary.ViewModels.PublicationHouseViewModels
{
    public class GetComplexPublicationHouseViewModel
    {
        public List<PublicationHouseGetComplexPublicationHouseViewModelItem> PublicationHouseList { get; set; }

        public GetComplexPublicationHouseViewModel()
        {
            PublicationHouseList = new List<PublicationHouseGetComplexPublicationHouseViewModelItem>();
        }
    }

    public class PublicationHouseGetComplexPublicationHouseViewModelItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public IEnumerable<BookViewModelItem> BookList { get; set; }
    }

    public class BookViewModelItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
    }
}
