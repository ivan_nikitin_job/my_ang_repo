﻿using System.Collections.Generic;

namespace AngularLibrary.ViewModels.PublicationHouseViewModels
{
    public class UpdatePublicationHouseRequestModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public IEnumerable<long> BookIdList { get; set; }

        public UpdatePublicationHouseRequestModel()
        {
            BookIdList = new List<long>();
        }
    }
}
