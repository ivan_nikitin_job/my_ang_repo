﻿using System.Collections.Generic;

namespace AngularLibrary.ViewModels.PublicationHouseViewModels
{
    public class GetSimplePublicationHouseViewModel
    {
        public List<PublicationHouseGetSimplePublicationHouseViewModelItem> PublicationHouseList { get; set; }

        public GetSimplePublicationHouseViewModel()
        {
            PublicationHouseList = new List<PublicationHouseGetSimplePublicationHouseViewModelItem>();
        }
    }

    public class PublicationHouseGetSimplePublicationHouseViewModelItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
