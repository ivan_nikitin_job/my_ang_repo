﻿namespace AngularLibrary.ViewModels.PublicationHouseViewModels
{
    public class RelationBookPublicationHouseViewModel
    {
        public long BookID { get; set; }
        public long PublicationHouseID { get; set; }
    }
}
