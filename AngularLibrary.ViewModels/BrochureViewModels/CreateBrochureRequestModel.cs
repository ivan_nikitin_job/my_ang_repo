﻿using AngularLibrary.ViewModels.Enums;

namespace AngularLibrary.ViewModels.BrochureViewModels
{
    public class CreateBrochureRequestModel
    {
        public string Name { get; set; }
        public long NumberOfPages { get; set; }
        public TypeOfCoverViewModel TypeOfCover { get; set; }
    }
}
