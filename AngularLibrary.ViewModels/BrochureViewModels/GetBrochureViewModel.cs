﻿using AngularLibrary.ViewModels.Enums;
using System.Collections.Generic;

namespace AngularLibrary.ViewModels.BrochureViewModels
{
    public class GetBrochureViewModel
    {
        public List<BrochureGetBrochureViewModelItem> BrochureList { get; set; }

        public GetBrochureViewModel()
        {
            BrochureList = new List<BrochureGetBrochureViewModelItem>();
        }
    }

    public class BrochureGetBrochureViewModelItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long NumberOfPages { get; set; }
        public TypeOfCoverViewModel TypeOfCover { get; set; }
    }
}
