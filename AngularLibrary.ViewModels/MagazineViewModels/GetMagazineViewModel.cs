﻿using System.Collections.Generic;

namespace AngularLibrary.ViewModels.MagazineViewModels
{
    public class GetMagazineViewModel
    {
        public List<MagazineGetMagazineViewModelItem> MagazineList { get; set; }

        public GetMagazineViewModel()
        {
            MagazineList = new List<MagazineGetMagazineViewModelItem>();
        }
    }

    public class MagazineGetMagazineViewModelItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Issue { get; set; }
        public int YearOfPublishing { get; set; }
    }
}
