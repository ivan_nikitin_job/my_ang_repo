﻿namespace AngularLibrary.ViewModels.MagazineViewModels
{
    public class UpdateMagazineRequestModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Issue { get; set; }
        public int YearOfPublishing { get; set; }
    }
}
