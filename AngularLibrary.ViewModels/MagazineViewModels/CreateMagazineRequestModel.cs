﻿namespace AngularLibrary.ViewModels.MagazineViewModels
{
    public class CreateMagazineRequestModel
    {
        public string Name { get; set; }
        public string Issue { get; set; }
        public int YearOfPublishing { get; set; }
    }
}
