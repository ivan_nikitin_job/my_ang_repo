﻿using AngularLibrary.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AngularLibrary.DataAccessLayer
{
    public class ApplicationContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationContext() : base() { }
        public ApplicationContext(DbContextOptions options) : base(options) { }

        public DbSet<Book> Book { get; set; }
        public DbSet<Brochure> Brochure { get; set; }
        public DbSet<Magazine> Magazine { get; set; }
        public DbSet<PublicationHouse> PublicationHouse { get; set; }
    }
}
