﻿using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.DataAccessLayer.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        Task<IEnumerable<TEntity>> Get();
        bool Create(TEntity item);
        bool Update(TEntity item);
        bool Delete(long id);
    }
}
