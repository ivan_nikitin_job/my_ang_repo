﻿using AngularLibrary.Entities;

namespace AngularLibrary.DataAccessLayer.Interfaces
{
    public interface IBrochureRepository : IBaseRepository<Brochure>
    {
    }
}
