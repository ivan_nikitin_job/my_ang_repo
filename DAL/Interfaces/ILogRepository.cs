﻿using AngularLibrary.Entities;

namespace AngularLibrary.DataAccessLayer.Interfaces
{
    public interface ILogRepository
    {
        void Log(Log log);
    }
}
