﻿using AngularLibrary.Entities;
using System.Collections.Generic;

namespace AngularLibrary.DataAccessLayer.Interfaces
{
    public interface IPublicationHouseRepository : IBaseRepository<PublicationHouse>
    {
        bool Update(PublicationHouse item, IEnumerable<long> relations);
        bool Create(PublicationHouse item, IEnumerable<long> relations);
    }
}