﻿using AngularLibrary.Entities;
using System.Collections.Generic;

namespace AngularLibrary.DataAccessLayer.Interfaces
{
    public interface IBookPublicationHouseRepository
    {
        IEnumerable<BookPublicationHouse> Get();
        IEnumerable<BookPublicationHouse> GetByPublicationHouseID(long id);
        IEnumerable<BookPublicationHouse> GetByBookID(long id);
    }
}