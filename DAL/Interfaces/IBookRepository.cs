﻿using AngularLibrary.Entities;
using System.Collections.Generic;

namespace AngularLibrary.DataAccessLayer.Interfaces
{
    public interface IBookRepository : IBaseRepository<Book>
    {
        bool Update(Book item, IEnumerable<long> relations);
        bool Create(Book item, IEnumerable<long> relations);
    }
}