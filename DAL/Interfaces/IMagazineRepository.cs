﻿using AngularLibrary.Entities;

namespace AngularLibrary.DataAccessLayer.Interfaces
{
    public interface IMagazineRepository : IBaseRepository<Magazine>
    {
    }
}
