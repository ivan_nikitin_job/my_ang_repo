﻿using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace AngularLibrary.DataAccessLayer.Repositories
{
    public class BookPublicationHouseRepository : IBookPublicationHouseRepository
    {
        private string _connectionString;
        private const string _tableName = "BookPublicationHouse";

        public BookPublicationHouseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<BookPublicationHouse> GetByPublicationHouseID(long id)
        {
            var query = $"SELECT * FROM {_tableName} WHERE PublicationHouseID = {id}";

            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Query<BookPublicationHouse>(query);

                return result;
            }
        }

        public IEnumerable<BookPublicationHouse> GetByBookID(long id)
        {
            var query = $"SELECT * FROM {_tableName} WHERE BookID = {id}";

            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Query<BookPublicationHouse>(query);

                return result;
            }
        }

        public IEnumerable<BookPublicationHouse> Get()
        {
            var query = $"SELECT * FROM {_tableName}";

            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Query<BookPublicationHouse>(query);

                return result;
            }
        }
    }
}
