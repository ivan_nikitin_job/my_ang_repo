﻿using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AngularLibrary.DataAccessLayer.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity, new()
    {
        protected string _connectionString;

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public virtual bool Create(TEntity item)
        {
            var name = typeof(TEntity).Name;
            var properties = typeof(TEntity).GetProperties();

            var propertiesString = new StringBuilder(128);
            var propertiesStringWithAtSign = new StringBuilder(128);

            for (int index = 0; index < properties.Length; index++)
            {
                if (properties[index].Name == "Id")
                {
                    continue;
                }

                propertiesString.Append(properties[index].Name);
                propertiesStringWithAtSign.Append('@').Append(properties[index].Name);

                if (index != properties.Length - 1)
                {
                    propertiesString.Append(", ");
                    propertiesStringWithAtSign.Append(", ");
                    continue;
                }
            }

            var query = $"INSERT INTO {name}({propertiesString}) VALUES ({propertiesStringWithAtSign}); SELECT CAST(SCOPE_IDENTITY() as bigint);";

            using (var connection = new SqlConnection(_connectionString))
            {
                var executeResult = connection.Execute(query.ToString(), item);

                bool result = (executeResult == 1);

                return result;
            }
        }

        public bool Delete(long id)
        {
            var name = typeof(TEntity).Name;

            var query = $"DELETE FROM {name} WHERE Id = {id}";

            using (var connection = new SqlConnection(_connectionString))
            {
                var executeResult = connection.Execute(query);

                bool result = (executeResult == 1);

                return result;
            }
        }

        public async Task<IEnumerable<TEntity>> Get()
        {
            IEnumerable<TEntity> result = new List<TEntity>();

            var name = typeof(TEntity).Name;

            var query = $"SELECT * FROM {name}";

            using (var connection = new SqlConnection(_connectionString))
            {
                result = await connection.QueryAsync<TEntity>(query);
            }

            return result;
        }

        public virtual bool Update(TEntity item)
        {
            var name = typeof(TEntity).Name;

            var properties = typeof(TEntity).GetProperties();

            var propertiesString = new StringBuilder(128);

            for (int index = 0; index < properties.Length; index++)
            {
                if (properties[index].Name == "Id")
                {
                    continue;
                }

                propertiesString.Append(properties[index].Name).Append(" = ").Append('@').Append(properties[index].Name);

                if (index != properties.Length - 1)
                {
                    propertiesString.Append(", ");
                }
            }

            var query = $"UPDATE {name} SET {propertiesString} WHERE Id = {item.Id}";

            using (var connection = new SqlConnection(_connectionString))
            {
                var executeResult = connection.Execute(query.ToString(), item);

                bool result = (executeResult == 1);

                return result;
            }
        }
    }
}
