﻿using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using Dapper;
using System.Data.SqlClient;

namespace AngularLibrary.DataAccessLayer.Repositories
{
    public class LogRepository : ILogRepository
    {
        protected string _connectionString;

        public LogRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Log(Log log)
        {
            var query = $"INSERT INTO Log (LogLevel, CategoryName, Message, UserName, Timestamp) VALUES (@LogLevel, @CategoryName, @Message, @UserName, @Timestamp);";

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute(query.ToString(), log);
            }
        }
    }
}