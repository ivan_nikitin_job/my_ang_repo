﻿using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace AngularLibrary.DataAccessLayer.Repositories
{
    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        private const string _bookTableName = "Book";
        private const string _bookPublicationHouseTableName = "BookPublicationHouse";

        public BookRepository(string connectionString) : base(connectionString)
        { }

        public bool Update(Book item, IEnumerable<long> publicationHouseIds)
        {
            var deleteRelationsQuery = $"DELETE FROM {_bookPublicationHouseTableName} WHERE BookId =  {item.Id}";
            var insertNewRelationsQuery = $"INSERT INTO {_bookPublicationHouseTableName} (BookId, PublicationHouseId) VALUES(@BookId, @PublicationHouseId)";
            var updateQuery = $"UPDATE {_bookTableName} SET Title = @Title, AuthorName = @AuthorName, YearOfPublish = @YearOfPublish WHERE Id = {item.Id}";

            var bookPublicationHouseList = new List<BookPublicationHouse>();

            foreach (var id in publicationHouseIds)
            {
                bookPublicationHouseList.Add(new BookPublicationHouse { BookId = item.Id, PublicationHouseId = id });
            }

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute(deleteRelationsQuery);

                var bookPublicationHouseUpdateResult = connection.Execute(insertNewRelationsQuery, bookPublicationHouseList);

                var updateResult = connection.Execute(updateQuery, item);

                bool result = (updateResult == 1 || bookPublicationHouseUpdateResult == bookPublicationHouseList.Count());

                return result;
            }
        }

        public bool Create(Book item, IEnumerable<long> publicationHouseIds)
        {
            var bookQuery = $"INSERT INTO {_bookTableName} (Title, AuthorName, YearOfPublish, CreationTime) VALUES(@Title, @AuthorName, @YearOfPublish, @CreationTime); SELECT CAST(SCOPE_IDENTITY() as bigint);";

            var bookPublicationHouseQuary = $"INSERT INTO {_bookPublicationHouseTableName} (BookId, PublicationHouseId) VALUES(@BookId, @PublicationHouseId)";

            using (var connection = new SqlConnection(_connectionString))
            {
                long bookId = connection.Query<long>(bookQuery, item).Single();

                var bookPublicationHouseList = new List<BookPublicationHouse>();

                if (publicationHouseIds != null)
                {
                    foreach (var id in publicationHouseIds)
                    {
                        bookPublicationHouseList.Add(new BookPublicationHouse { BookId = bookId, PublicationHouseId = id });
                    }
                }

                var executeResult = connection.Execute(bookPublicationHouseQuary, bookPublicationHouseList);

                bool result = (executeResult == bookPublicationHouseList.Count() || bookId > 0);

                return result;
            }
        }
    }
}
