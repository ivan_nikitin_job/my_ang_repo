﻿using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;

namespace AngularLibrary.DataAccessLayer.Repositories
{
    public class MagazineRepository : BaseRepository<Magazine>, IMagazineRepository
    {
        public MagazineRepository(string connectionString) : base(connectionString)
        {
        }
    }
}
