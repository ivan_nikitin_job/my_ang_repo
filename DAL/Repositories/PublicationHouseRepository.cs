﻿using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace AngularLibrary.DataAccessLayer.Repositories
{
    public class PublicationHouseRepository : BaseRepository<PublicationHouse>, IPublicationHouseRepository
    {
        private const string _publicationHouseTableName = "PublicationHouse";
        private const string _bookPublicationHouseTableName = "BookPublicationHouse";

        public PublicationHouseRepository(string connectionString) : base(connectionString)
        { }

        public bool Update(PublicationHouse item, IEnumerable<long> bookIds)
        {
            var deleteBookPublicationHouseQuery = $"DELETE FROM {_bookPublicationHouseTableName} WHERE PublicationHouseId = {item.Id}";
            var insertNewBookPublicationHouseQuery = $"INSERT INTO {_bookPublicationHouseTableName} (BookId, PublicationHouseId) VALUES(@BookId, @PublicationHouseId)";
            var updateQuery = $"UPDATE {_publicationHouseTableName} SET Name = @Name, Address = @Address WHERE Id = {item.Id}";

            var bookPublicationHouseList = new List<BookPublicationHouse>();

            foreach (var id in bookIds)
            {
                bookPublicationHouseList.Add(new BookPublicationHouse { BookId = id, PublicationHouseId = item.Id });
            }

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute(deleteBookPublicationHouseQuery);

                var bookPublicationHouseUpdateResult = connection.Execute(insertNewBookPublicationHouseQuery, bookPublicationHouseList);

                var updateResult = connection.Execute(updateQuery, item);

                bool result = (bookPublicationHouseUpdateResult == bookPublicationHouseList.Count() || updateResult == 1);

                return result;
            }
        }

        public bool Create(PublicationHouse item, IEnumerable<long> bookIds)
        {
            var publicationHouseQuery = $"INSERT INTO {_publicationHouseTableName} (Name, Address, CreationTime) VALUES(@Name, @Address, @CreationTime); SELECT CAST(SCOPE_IDENTITY() as bigint);";
            var bookPublicationHouseQuery = $"INSERT INTO {_bookPublicationHouseTableName} (BookId, PublicationHouseId) VALUES(@BookId, @PublicationHouseId)";

            using (var connection = new SqlConnection(_connectionString))
            {
                long publicationHouseId = connection.Query<long>(publicationHouseQuery, item).FirstOrDefault();

                var bookPublicationHouseList = new List<BookPublicationHouse>();

                foreach (var id in bookIds)
                {
                    bookPublicationHouseList.Add(new BookPublicationHouse { BookId = id, PublicationHouseId = publicationHouseId });
                }

                var executeResult = connection.Execute(bookPublicationHouseQuery, bookPublicationHouseList);

                bool result = (executeResult == bookPublicationHouseList.Count() || publicationHouseId > 0);

                return result;
            }
        }
    }
}
