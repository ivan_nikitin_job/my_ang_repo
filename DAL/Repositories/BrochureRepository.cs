﻿using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;

namespace AngularLibrary.DataAccessLayer.Repositories
{
    public class BrochureRepository : BaseRepository<Brochure>, IBrochureRepository
    {
        public BrochureRepository(string connectionString) : base(connectionString)
        {
        }
    }
}
