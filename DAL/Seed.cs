﻿using AngularLibrary.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace AngularLibrary.DataAccessLayer
{
    public class Seed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var books = new[]
            {
                new Book { AuthorName = "author1", Title = "title1", YearOfPublish = 1941 },
                new Book { AuthorName = "author2", Title = "titl12", YearOfPublish = 229 }
            };

            var brochures = new[]
            {
                new Brochure { TypeOfCover = TypeOfCover.Soft, Name = "name1", NumberOfPages = 15 },
                new Brochure { TypeOfCover = TypeOfCover.Hard, Name = "name2", NumberOfPages = 100500 }
            };

            var magazines = new[]
            {
                new Magazine { Issue = "issue1", Name = "name1", YearOfPublishing =1999 },
                new Magazine { Issue = "issue2", Name = "name2", YearOfPublishing = 11212 }
            };

            var publicationHouses = new[]
            {
                new PublicationHouse { Name = "name1", Address = "adr1" },
                new PublicationHouse { Name = "name2", Address = "adr2" }
            };

            var role1 = new IdentityRole("AppUser");
            var role2 = new IdentityRole("AppAdmin");

            using (var context = new ApplicationContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationContext>>()))
            {
                if (!context.Book.Any())
                {
                    context.Book.AddRange(books);
                }

                if (!context.Brochure.Any())
                {
                    context.Brochure.AddRange(brochures);
                }

                if (!context.Magazine.Any())
                {
                    context.Magazine.AddRange(magazines);
                }

                if (!context.PublicationHouse.Any())
                {
                    context.PublicationHouse.AddRange(publicationHouses);
                }

                if (!context.Book.Any() && !context.PublicationHouse.Any())
                {
                    context.AddRange
                    (
                        new BookPublicationHouse { Book = books[0], PublicationHouse = publicationHouses[0] },
                        new BookPublicationHouse { Book = books[0], PublicationHouse = publicationHouses[1] },
                        new BookPublicationHouse { Book = books[1], PublicationHouse = publicationHouses[0] },
                        new BookPublicationHouse { Book = books[1], PublicationHouse = publicationHouses[1] }
                    );
                }

                if (!context.Roles.Any())
                {
                    role1.NormalizedName = role1.Name.Normalize();
                    context.Roles.Add(role1);
                    role2.NormalizedName = role2.Name.Normalize();
                    context.Roles.Add(role2);
                }

                context.SaveChanges();
            }
        }
    }
}
