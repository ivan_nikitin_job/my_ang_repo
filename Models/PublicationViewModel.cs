﻿using System.Collections.Generic;

namespace AngularLibrary.ViewModels
{
    public class PublicationViewItem
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class PublicationViewModel
    {
        public List<PublicationViewItem> PublicationList { get; set; }

        public PublicationViewModel()
        {
            PublicationList = new List<PublicationViewItem>();
        }
    }
}
