namespace AngularLibrary.Models.RequestModels.AuthorizationRequestModels
{
    public class AuthorizationRequestModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string EMail { get; set; }
    }
}
