﻿using System.Collections.Generic;

namespace AngularLibrary.Models.RequestModels.BookRequestModels
{
    public class UpdateBookRequestModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public int YearOfPublish { get; set; }

        public IEnumerable<long> PublicationHouseIdList { get; set; }

        public UpdateBookRequestModel()
        {
            PublicationHouseIdList = new List<long>();
        }
    }
}
