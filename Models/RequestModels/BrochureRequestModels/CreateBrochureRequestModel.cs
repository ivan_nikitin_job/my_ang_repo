﻿using AngularLibrary.Entities;

namespace AngularLibrary.Models.RequestModels.BrochureRequestModels
{
    public class CreateBrochureRequestModel
    {
        public string Name { get; set; }
        public long NumberOfPages { get; set; }
        public TypeOfCover TypeOfCover { get; set; }
    }
}
