﻿using AngularLibrary.Entities;

namespace AngularLibrary.Models.RequestModels.BrochureRequestModels
{
    public class UpdateBrochureRequestModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long NumberOfPages { get; set; }
        public TypeOfCover TypeOfCover { get; set; }
    }
}
