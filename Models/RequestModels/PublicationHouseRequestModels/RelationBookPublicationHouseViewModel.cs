﻿namespace AngularLibrary.Models.RequestModels.PublicationHouseRequestModels
{
    public class RelationBookPublicationHouseViewModel
    {
        public long BookID { get; set; }
        public long PublicationHouseID { get; set; }
    }
}
