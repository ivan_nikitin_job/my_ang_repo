﻿using System.Collections.Generic;

namespace AngularLibrary.Models.RequestModels.PublicationHouseRequestModels
{
    public class CreatePublicationHouseRequestModel
    {
        public string Name { get; set; }
        public string Address { get; set; }

        public IEnumerable<long> BookIdList { get; set; }

        public CreatePublicationHouseRequestModel()
        {
            BookIdList = new List<long>();
        }
    }
}
