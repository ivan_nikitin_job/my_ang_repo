﻿using System;

namespace AngularLibrary.Entities
{
    public class Log
    {
        public string LogLevel { get; set; }
        public string CategoryName { get; set; }
        public string Message { get; set; }
        public string UserName { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
