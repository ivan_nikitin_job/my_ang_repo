﻿namespace AngularLibrary.Entities
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public int YearOfPublish { get; set; }
    }
}
