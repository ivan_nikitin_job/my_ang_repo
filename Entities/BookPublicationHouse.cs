﻿namespace AngularLibrary.Entities
{
    public class BookPublicationHouse
    {
        public long BookId { get; set; }
        public virtual Book Book { get; set; }
        public long PublicationHouseId { get; set; }
        public virtual PublicationHouse PublicationHouse { get; set; }
    }
}
