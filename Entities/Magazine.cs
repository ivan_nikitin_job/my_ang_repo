﻿namespace AngularLibrary.Entities
{
    public class Magazine : BaseEntity
    {
        public string Name { get; set; }
        public string Issue { get; set; }
        public int YearOfPublishing { get; set; }
    }
}