﻿using System;

namespace AngularLibrary.Entities
{
    public abstract class BaseEntity
    {
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }

        public BaseEntity()
        {
            CreationTime = DateTime.UtcNow;
        }
    }
}
