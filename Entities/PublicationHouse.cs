﻿namespace AngularLibrary.Entities
{
    public class PublicationHouse : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
