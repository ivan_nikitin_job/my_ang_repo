﻿namespace AngularLibrary.Entities
{
    public enum TypeOfCover
    {
        Hard = 0,
        Soft = 1
    }
}
