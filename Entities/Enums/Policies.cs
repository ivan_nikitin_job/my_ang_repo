﻿namespace AngularLibrary.Entities.Enums
{
    public enum Policies
    {
        DefaultPolicy = 0,
        AdminRolePolicy = 1,
        UserRolePolicy = 2
    }
}
