﻿namespace AngularLibrary.Entities
{
    public class Brochure : BaseEntity
    {
        public string Name { get; set; }
        public TypeOfCover TypeOfCover { get; set; }
        public long NumberOfPages { get; set; }
    }
}