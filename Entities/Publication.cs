﻿namespace AngularLibrary.Entities
{
    public class Publication : BaseEntity
    {
        public string Name { get; set; }
    }
}
