using AngularLibrary.BLL;
using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.Configs;
using AngularLibrary.DataAccessLayer;
using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.DataAccessLayer.Repositories;
using AngularLibrary.JWT.Configurations;
using AngularLibrary.JWT.Helpers;
using AngularLibrary.JWT.TokenGeneration;
using AngularLibrary.Logging;
using AngularLibrary.Services;
using AngularLibrary.Services.Interfaces;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Text;

namespace AngularLibrary
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private static SymmetricSecurityKey _signingKey;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("ApplicationContext");
            services.AddDbContext<ApplicationContext>(opttions => opttions
                                                        .UseSqlServer(
                                                        connection,
                                                        b => b.MigrationsAssembly("DAL")));

            var keyConfiguration = Configuration.GetSection("SecretKey");
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(keyConfiguration["Key"]));

            var applicationSettingSection = Configuration.GetSection("ApplicationConfigurations");
            var applicationConfiguration = applicationSettingSection.Get<ApplicationConfigurations>();
            applicationConfiguration.JwtConfiguration.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);

            services.Configure<ApplicationConfigurations>(applicationSettingSection);
            services.Configure<ApplicationConfigurations>(opttions =>
            {
                opttions.JwtConfiguration.SigningCredentials = applicationConfiguration.JwtConfiguration.SigningCredentials;
            });

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = applicationConfiguration.PasswordConfiguration.RequireDigit;
                options.Password.RequiredLength = applicationConfiguration.PasswordConfiguration.RequiredLength;
                options.Password.RequiredUniqueChars = applicationConfiguration.PasswordConfiguration.RequiredUniqueChars;
                options.Password.RequireUppercase = applicationConfiguration.PasswordConfiguration.RequireUppercase;
                options.Password.RequireLowercase = applicationConfiguration.PasswordConfiguration.RequireLowercase;
                options.Password.RequireNonAlphanumeric = applicationConfiguration.PasswordConfiguration.RequireNonAlphanumeric;
            })
            .AddEntityFrameworkStores<ApplicationContext>()
            .AddDefaultTokenProviders();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var tokenValidationParams = new TokenValidationParameters
            {
                ValidateIssuer = applicationConfiguration.JwtConfiguration.ValidateIssuer,
                ValidIssuer = applicationConfiguration.JwtConfiguration.Issuer,

                ValidateAudience = applicationConfiguration.JwtConfiguration.ValidateAudience,
                ValidAudience = applicationConfiguration.JwtConfiguration.Audience,

                ValidateIssuerSigningKey = applicationConfiguration.JwtConfiguration.ValidateIssuerSigningKey,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = applicationConfiguration.JwtConfiguration.RequireExpirationTime,
                ValidateLifetime = applicationConfiguration.JwtConfiguration.ValidateLifetime,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(confOptions =>
            {
                confOptions.ClaimsIssuer = applicationConfiguration.JwtConfiguration.Issuer;
                confOptions.TokenValidationParameters = tokenValidationParams;
                confOptions.SaveToken = false;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Entities.Enums.Policies.DefaultPolicy.ToString(), policy =>
                    policy.RequireClaim(
                        JWTStrings.ClaimIdentifierRole,
                        JWTStrings.IdentifierRoleAdmin,
                        JWTStrings.IdentifierRoleUser
                    ));
                options.AddPolicy(Entities.Enums.Policies.AdminRolePolicy.ToString(), policy =>
                    policy.RequireClaim(
                        JWTStrings.ClaimIdentifierRole,
                        JWTStrings.IdentifierRoleAdmin
                    ));
                options.AddPolicy(Entities.Enums.Policies.UserRolePolicy.ToString(), policy =>
                    policy.RequireClaim(
                        JWTStrings.ClaimIdentifierRole,
                        JWTStrings.IdentifierRoleUser
                    ));
            });

            services.AddMvcCore().AddAuthorization().AddJsonFormatters();

            services.AddLogging(builder =>
            {
                builder.AddProvider(new LoggerToDBProvider(new LogRepository(applicationConfiguration.DapperConnectionString)));
            });

            services.AddSingleton(Configuration);

            services.AddTransient(typeof(UserManager<IdentityUser>));

            services.AddSingleton(typeof(ApplicationConfigurations), applicationConfiguration);
            services.AddSingleton(typeof(JwtConfigurations), applicationConfiguration.JwtConfiguration);
            services.AddSingleton(typeof(string), applicationConfiguration.DapperConnectionString);

            services.AddSingleton<IJwtFactory, JwtFactory>();

            services.AddSingleton<IBookPublicationHouseRepository, BookPublicationHouseRepository>();
            services.AddSingleton<IBookRepository, BookRepository>();
            services.AddSingleton<IBrochureRepository, BrochureRepository>();
            services.AddSingleton<IMagazineRepository, MagazineRepository>();
            services.AddSingleton<IPublicationHouseRepository, PublicationHouseRepository>();
            services.AddSingleton<ILogRepository, LogRepository>();

            services.AddSingleton<IBookBuisnessLogic, BookBusinessLogic>();
            services.AddSingleton<IBookPublicationHouseBusinessLogic, BookPublicationHouseBusinessLogic>();
            services.AddSingleton<IBrochureBusinessLogic, BrochureBusinessLogic>();
            services.AddSingleton<IMagazineBusinessLogic, MagazineBusinessLogic>();
            services.AddSingleton<IPublicationHouseBusinessLogic, PublicationHouseBusinessLogic>();

            services.AddTransient<IApplicationAuthorizationService, ApplicationAuthorizationService>();
            services.AddSingleton<IBookService, BookService>();
            services.AddSingleton<IBrochureService, BrochureService>();
            services.AddSingleton<IMagazineService, MagazineService>();
            services.AddSingleton<IPublicationHouseService, PublicationHouseService>();

            services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
        }

        public void Configure(IApplicationBuilder applicationBuilder, IHostingEnvironment environment)
        {
            applicationBuilder.UseDeveloperExceptionPage();
            applicationBuilder.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == (int)HttpStatusCode.NotFound &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            applicationBuilder.UseAuthentication();
            applicationBuilder.UseDefaultFiles();
            applicationBuilder.UseStaticFiles();
            applicationBuilder.UseMvc();
            applicationBuilder.UseMvcWithDefaultRoute();
        }
    }
}
