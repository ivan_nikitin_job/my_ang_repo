using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using Microsoft.Extensions.Logging;
using System;

namespace AngularLibrary.Logging
{
    public class MyDBLogger : ILogger
    {
        private readonly string _categoryName;
        private readonly ILogRepository _repository;

        public MyDBLogger(string categoryName, ILogRepository repository)
        {
            _repository = repository;
            _categoryName = categoryName;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (exception == null)
            {
                exception = new Exception("Empty Exception");
            }

            if ((logLevel == LogLevel.Critical) || (logLevel == LogLevel.Warning) || (logLevel == LogLevel.Error))
            {
                RecordMsg(logLevel, state, exception, formatter);
            }
        }

        private void RecordMsg<TState>(LogLevel logLevel, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var logItem = new Log
            {
                LogLevel = logLevel.ToString(),
                CategoryName = _categoryName,
                Message = $"{state} {exception.ToString()}",
                Timestamp = DateTime.Now
            };

            _repository.Log(logItem);
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return new NoopDisposable();
        }

        private class NoopDisposable : IDisposable
        {
            public void Dispose()
            {
            }
        }
    }
}
