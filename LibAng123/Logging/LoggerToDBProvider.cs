using AngularLibrary.DataAccessLayer.Interfaces;
using Microsoft.Extensions.Logging;

namespace AngularLibrary.Logging
{
    public class LoggerToDBProvider : ILoggerProvider
    {
        private ILogRepository _repository;

        public LoggerToDBProvider(ILogRepository repository)
        {
            _repository = repository;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new MyDBLogger(categoryName, _repository);
        }

        public void Dispose()
        {
        }
    }
}
