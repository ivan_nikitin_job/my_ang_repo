using AngularLibrary.JWT.Configurations;

namespace AngularLibrary.Configs
{
    public class ApplicationConfigurations
    {
        public JwtConfigurations JwtConfiguration { get; set; }
        public PasswordConfigurations PasswordConfiguration { get; set; }
        public string DapperConnectionString { get; set; }
    }
}
