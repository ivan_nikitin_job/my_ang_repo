webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/authorization/authorization.module": [
		"../../../../../src/app/components/authorization/authorization.module.ts",
		"authorization.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var kendo_angular_grid_1 = __webpack_require__("../../../../@progress/kendo-angular-grid/dist/es/index.js");
var kendo_angular_dropdowns_1 = __webpack_require__("../../../../@progress/kendo-angular-dropdowns/dist/es/index.js");
var ngx_permissions_1 = __webpack_require__("../../../../ngx-permissions/ngx-permissions.umd.js");
var book_list_component_1 = __webpack_require__("../../../../../src/app/components/book/book-list/book-list.component.ts");
var magazine_list_component_1 = __webpack_require__("../../../../../src/app/components/magazine/magazine-list/magazine-list.component.ts");
var brochure_list_component_1 = __webpack_require__("../../../../../src/app/components/brochure/brochure-list/brochure-list.component.ts");
var publication_house_list_component_1 = __webpack_require__("../../../../../src/app/components/publication-house/publication-house-list/publication-house-list.component.ts");
var appRoutes = [
    { path: '', component: book_list_component_1.BookListComponent, pathMatch: 'full' },
    { path: 'magazine', component: magazine_list_component_1.MagazineListComponent, pathMatch: 'full' },
    { path: 'brochure', component: brochure_list_component_1.BrochureListComponent, pathMatch: 'full' },
    { path: 'publicationHouse', component: publication_house_list_component_1.PublicationHouseListComponent, pathMatch: 'full' },
    { path: 'authorization', loadChildren: './components/authorization/authorization.module#AuthorizationModule' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                kendo_angular_grid_1.GridModule,
                kendo_angular_dropdowns_1.DropDownsModule,
                router_1.RouterModule.forRoot(appRoutes),
                ngx_permissions_1.NgxPermissionsModule.forRoot()
            ],
            exports: [
                router_1.RouterModule
            ],
            declarations: [
                book_list_component_1.BookListComponent,
                magazine_list_component_1.MagazineListComponent,
                brochure_list_component_1.BrochureListComponent,
                publication_house_list_component_1.PublicationHouseListComponent
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class='container-fluid'>\r\n  <div class='row'>\r\n    <div class='col-sm-3 navbar'>\r\n      <app-nav-menu></app-nav-menu>\r\n    </div>\r\n    <div class='col-sm-9 body-content'>\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var http_2 = __webpack_require__("../../../http/esm5/http.js");
var animations_1 = __webpack_require__("../../../platform-browser/esm5/animations.js");
var window_1 = __webpack_require__("../../../../../src/app/window.ts");
var token_interceptor_1 = __webpack_require__("../../../../../src/app/token.interceptor.ts");
var nav_menu_component_1 = __webpack_require__("../../../../../src/app/shared/nav-menu/nav-menu.component.ts");
var ngx_permissions_1 = __webpack_require__("../../../../ngx-permissions/ngx-permissions.umd.js");
var kendo_angular_grid_1 = __webpack_require__("../../../../@progress/kendo-angular-grid/dist/es/index.js");
var kendo_angular_dropdowns_1 = __webpack_require__("../../../../@progress/kendo-angular-dropdowns/dist/es/index.js");
var books_service_1 = __webpack_require__("../../../../../src/app/services/books.service.ts");
var magazine_service_1 = __webpack_require__("../../../../../src/app/services/magazine.service.ts");
var brochure_service_1 = __webpack_require__("../../../../../src/app/services/brochure.service.ts");
var publication_house_service_1 = __webpack_require__("../../../../../src/app/services/publication-house.service.ts");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var app_routing_module_1 = __webpack_require__("../../../../../src/app/app-routing.module.ts");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                nav_menu_component_1.NavMenuComponent
            ],
            imports: [
                platform_browser_1.BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
                forms_1.FormsModule,
                kendo_angular_grid_1.GridModule,
                kendo_angular_dropdowns_1.DropDownsModule,
                http_1.HttpClientModule,
                http_2.HttpModule,
                animations_1.BrowserAnimationsModule,
                app_routing_module_1.AppRoutingModule,
                ngx_permissions_1.NgxPermissionsModule.forRoot()
            ],
            providers: [
                http_1.HttpClientModule,
                http_2.HttpModule,
                window_1.windowProvider,
                books_service_1.BooksService,
                magazine_service_1.MagazineService,
                brochure_service_1.BrochureService,
                publication_house_service_1.PublicationHouseService,
                authorization_service_1.AuthorizationService,
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: token_interceptor_1.TokenInterceptor,
                    multi: true
                }
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/components/book/book-list/book-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/book/book-list/book-list.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<h2>Books</h2>\r\n\r\n<div *ngxPermissionsOnly=\"['user']\"> user mode</div>\r\n<div *ngxPermissionsOnly=\"['admin']\"> admin mode</div>\r\n\r\n<form *ngxPermissionsOnly=\"['admin','user']\" novalidate #myForm=\"ngForm\">\r\n    <kendo-grid [data]=\"view | async\"\r\n                [height]=\"550\"\r\n                (dataStateChange)=\"onStateChange($event)\"\r\n                (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\r\n                (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\r\n                [navigable]=\"true\"\r\n                (add)=\"addHandler($event, myForm)\">\r\n        <ng-template kendoGridToolbarTemplate *ngxPermissionsOnly=\"['admin']\">\r\n            <button kendoGridAddCommand type=\"button\" class=\"btn\">Add new</button>\r\n        </ng-template>\r\n        <kendo-grid-column field=\"id\" title=\"ID\" width=\"80\" editable=\"false\" nullable=\"false\">\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"title\" title=\"Title\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.title\" kendoGridFocusable name=\"title\" class=\"k-textbox\" placeholder=\"Title\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"authorName\" title=\"Author Name\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.authorName\" kendoGridFocusable name=\"authorName\" class=\"k-textbox\" placeholder=\"Author name\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"yearOfPublish\" title=\"Year Of Publish\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.yearOfPublish\" type=\"number\" kendoGridFocusable name=\"yearOfPublish\" class=\"k-textbox\" placeholder=\"YearOfPublish\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"publicationHouseList\" title=\"Publishing in\" width=\"200\">\r\n            <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n                <p *ngFor=\"let p of dataItem.publicationHouseList\">{{p.name}}</p>\r\n            </ng-template>\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <kendo-multiselect [data]=\"allPublicationHouseList\"\r\n                                   [(ngModel)]=\"selectedPublicationHouseList\"\r\n                                   [textField]=\"'name'\"\r\n                                   [valueField]=\"'id'\"\r\n                                   [placeholder]=\"'Select some...'\"\r\n                                   [ngModelOptions]=\"{standalone: true}\">\r\n                </kendo-multiselect>\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-command-column *ngxPermissionsOnly=\"['admin']\" title=\" \" width=\"220\">\r\n            <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n                <button kendoGridEditCommand type=\"button\" [primary]=\"true\">Edit</button>\r\n                <button kendoGridRemoveCommand type=\"button\">Remove</button>\r\n                <button kendoGridSaveCommand type=\"button\" [disabled]=\"myForm.invalid\"> {{ isNew ? 'Add' : 'Update' }} </button>\r\n                <button kendoGridCancelCommand type=\"button\">{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n            </ng-template>\r\n        </kendo-grid-command-column>\r\n    </kendo-grid>\r\n</form>\r\n\r\n<div *ngxPermissionsExcept=\"['admin','user']\">\r\n    <h1>Login to see more</h1>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/book/book-list/book-list.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var books_service_1 = __webpack_require__("../../../../../src/app/services/books.service.ts");
var publication_house_request_model_1 = __webpack_require__("../../../../../src/app/models/publication-house-request.model.ts");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var ngx_permissions_1 = __webpack_require__("../../../../ngx-permissions/ngx-permissions.umd.js");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var BookListComponent = /** @class */ (function () {
    function BookListComponent(bookServiceFactory, router, permissionsService) {
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.allPublicationHouseList = new Array();
        this.selectedPublicationHouseList = new Array();
        this._bookService = bookServiceFactory;
        this._permissionsService = permissionsService;
    }
    BookListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var permission = authorization_service_1.AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);
        this.view = this._bookService.pipe(map_1.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); }));
        this._bookService.read();
    };
    BookListComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this._bookService.read();
    };
    BookListComponent.prototype.addHandler = function (_a, formInstance) {
        var _this = this;
        var sender = _a.sender;
        formInstance.reset();
        this.closeEditor(sender);
        this._bookService.loadAllPublicationHouses().subscribe(function (data) { return _this.allPublicationHouseList = data; });
        this.selectedPublicationHouseList = [];
        sender.addRow(new publication_house_request_model_1.PublicationHouseRequestModel());
    };
    BookListComponent.prototype.editHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this._editedProduct = Object.assign({}, dataItem);
        this._bookService.loadAllPublicationHouses().subscribe(function (data) { return _this.allPublicationHouseList = data; });
        this._bookService.loadPublicationHouseId(this._editedProduct.id).subscribe(function (data) {
            _this.selectedPublicationHouseList = data;
        });
        sender.editRow(rowIndex);
    };
    BookListComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    BookListComponent.prototype.saveHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem, isNew = _a.isNew;
        this._bookService.save(dataItem, this.selectedPublicationHouseList, isNew);
        sender.closeRow(rowIndex);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    BookListComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this._bookService.remove(dataItem);
    };
    BookListComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this._editedRowIndex; }
        grid.closeRow(rowIndex);
        this._bookService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    BookListComponent = __decorate([
        core_1.Component({
            selector: 'book-list',
            template: __webpack_require__("../../../../../src/app/components/book/book-list/book-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/book/book-list/book-list.component.css")]
        }),
        __param(0, core_1.Inject(books_service_1.BooksService)),
        __metadata("design:paramtypes", [Object, router_1.Router,
            ngx_permissions_1.NgxPermissionsService])
    ], BookListComponent);
    return BookListComponent;
}());
exports.BookListComponent = BookListComponent;


/***/ }),

/***/ "../../../../../src/app/components/brochure/brochure-list/brochure-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/brochure/brochure-list/brochure-list.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Brochures</h2>\r\n\r\n<div *ngxPermissionsOnly=\"['user']\"> user mode</div>\r\n<div *ngxPermissionsOnly=\"['admin']\"> admin mode</div>\r\n\r\n<form *ngxPermissionsOnly=\"['user','admin']\" novalidate #myForm=\"ngForm\">\r\n    <kendo-grid [data]=\"view | async\"\r\n                [height]=\"550\"\r\n                (dataStateChange)=\"onStateChange($event)\"\r\n                (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\r\n                (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\r\n                (add)=\"addHandler($event, myForm)\">\r\n        <ng-template kendoGridToolbarTemplate *ngxPermissionsOnly=\"['admin']\">\r\n            <button kendoGridAddCommand type=\"button\" class=\"btn\">Add new</button>\r\n        </ng-template>\r\n        <kendo-grid-column field=\"id\" title=\"ID\" width=\"40\" editable=\"false\" nullable=\"false\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.id\" kendoGridFocusable name=\"id\" class=\"k-textbox\" hidden />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"name\" title=\"Name\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.name\" kendoGridFocusable name=\"name\" class=\"k-textbox\" placeholder=\"Name\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"numberOfPages\" title=\"Number of Pages\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.numberOfPages\" type=\"number\" kendoGridFocusable name=\"numberOfPages\" class=\"k-textbox\" placeholder=\"Number of pages\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"typeOfCover\" title=\"Type of Cover\" width=\"140\">\r\n            <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n                <span>{{TypeOfCover[dataItem.typeOfCover]}}</span>\r\n            </ng-template>\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <select [(ngModel)]=\"dataItem.typeOfCover\" kendoGridFocusable name=\"typeOfCover\" class=\"k-textbox\" placeholder=\"Type Of Cover\">\r\n                    <option value=\"0\">Hard</option>\r\n                    <option value=\"1\">Soft</option>\r\n                </select>\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-command-column *ngxPermissionsOnly=\"['admin']\" title=\" \" width=\"220\">\r\n            <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n                <button kendoGridEditCommand type=\"button\" [primary]=\"true\">Edit</button>\r\n                <button kendoGridRemoveCommand type=\"button\">Remove</button>\r\n                <button kendoGridSaveCommand type=\"button\" [disabled]=\"myForm.invalid || myForm.pristine\"> {{ isNew ? 'Add' : 'Update' }} </button>\r\n                <button kendoGridCancelCommand type=\"button\">{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n            </ng-template>\r\n        </kendo-grid-command-column>\r\n    </kendo-grid>\r\n</form>\r\n\r\n<div *ngxPermissionsExcept=\"['admin','user']\">\r\n    <h1>Login to see more</h1>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/brochure/brochure-list/brochure-list.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var brochure_service_1 = __webpack_require__("../../../../../src/app/services/brochure.service.ts");
var brochure_model_1 = __webpack_require__("../../../../../src/app/models/brochure.model.ts");
var brochure_type_of_cover_1 = __webpack_require__("../../../../../src/app/shared/brochure-type-of-cover.ts");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var ngx_permissions_1 = __webpack_require__("../../../../ngx-permissions/ngx-permissions.umd.js");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var BrochureListComponent = /** @class */ (function () {
    function BrochureListComponent(brochureServiceFactory, permissionsService) {
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.TypeOfCover = brochure_type_of_cover_1.TypeOfCover;
        this._brochureService = brochureServiceFactory;
        this._permissionsService = permissionsService;
    }
    BrochureListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var permission = authorization_service_1.AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);
        this.view = this._brochureService.pipe(map_1.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); }));
        this._brochureService.read();
    };
    BrochureListComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this._brochureService.read();
    };
    BrochureListComponent.prototype.addHandler = function (_a, formInstance) {
        var sender = _a.sender;
        formInstance.reset();
        this.closeEditor(sender);
        sender.addRow(new brochure_model_1.BrochureItem());
    };
    BrochureListComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this._editedRowIndex = rowIndex;
        this._editedProduct = Object.assign({}, dataItem);
        sender.editRow(rowIndex);
    };
    BrochureListComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    BrochureListComponent.prototype.saveHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem, isNew = _a.isNew;
        this._brochureService.save(dataItem, isNew);
        sender.closeRow(rowIndex);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    BrochureListComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this._brochureService.remove(dataItem);
    };
    BrochureListComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this._editedRowIndex; }
        grid.closeRow(rowIndex);
        this._brochureService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    BrochureListComponent = __decorate([
        core_1.Component({
            selector: 'brochure-list',
            template: __webpack_require__("../../../../../src/app/components/brochure/brochure-list/brochure-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/brochure/brochure-list/brochure-list.component.css")]
        }),
        __param(0, core_1.Inject(brochure_service_1.BrochureService)),
        __metadata("design:paramtypes", [Object, ngx_permissions_1.NgxPermissionsService])
    ], BrochureListComponent);
    return BrochureListComponent;
}());
exports.BrochureListComponent = BrochureListComponent;


/***/ }),

/***/ "../../../../../src/app/components/magazine/magazine-list/magazine-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/magazine/magazine-list/magazine-list.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<h2>Magazines</h2>\r\n\r\n<div *ngxPermissionsOnly=\"['user']\"> user mode</div>\r\n<div *ngxPermissionsOnly=\"['admin']\"> admin mode</div>\r\n\r\n<form *ngxPermissionsOnly=\"['admin','user']\" novalidate #myForm=\"ngForm\">\r\n    <kendo-grid [data]=\"view | async\"\r\n                [height]=\"550\"\r\n                (dataStateChange)=\"onStateChange($event)\"\r\n                (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\r\n                (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\r\n                (add)=\"addHandler($event, myForm)\">\r\n        <ng-template kendoGridToolbarTemplate *ngxPermissionsOnly=\"['admin']\">\r\n            <button kendoGridAddCommand type=\"button\" class=\"btn\">Add new</button>\r\n        </ng-template>\r\n        <kendo-grid-column field=\"id\" title=\"ID\" width=\"40\" editable=\"false\" nullable=\"false\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.id\" kendoGridFocusable name=\"id\" class=\"k-textbox\" hidden />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"name\" title=\"Name\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.name\" kendoGridFocusable name=\"name\" class=\"k-textbox\" placeholder=\"Name\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"issue\" title=\"issue\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.issue\" kendoGridFocusable name=\"issue\" class=\"k-textbox\" placeholder=\"Issue\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"yearOfPublishing\" title=\"Year Of Publish\" width=\"140\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.yearOfPublishing\" type=\"number\" kendoGridFocusable name=\"yearOfPublishing\" class=\"k-textbox\" placeholder=\"Year Of publishing\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-command-column *ngxPermissionsOnly=\"['admin']\" title=\" \" width=\"220\">\r\n            <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n                <button kendoGridEditCommand type=\"button\" [primary]=\"true\">Edit</button>\r\n                <button kendoGridRemoveCommand type=\"button\">Remove</button>\r\n                <button kendoGridSaveCommand type=\"button\" [disabled]=\"myForm.invalid || myForm.pristine\"> {{ isNew ? 'Add' : 'Update' }} </button>\r\n                <button kendoGridCancelCommand type=\"button\">{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n            </ng-template>\r\n        </kendo-grid-command-column>\r\n    </kendo-grid>\r\n</form>\r\n\r\n<div *ngxPermissionsExcept=\"['admin','user']\">\r\n    <h1>Login to see more</h1>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/magazine/magazine-list/magazine-list.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var magazine_service_1 = __webpack_require__("../../../../../src/app/services/magazine.service.ts");
var magazine_model_1 = __webpack_require__("../../../../../src/app/models/magazine.model.ts");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var ngx_permissions_1 = __webpack_require__("../../../../ngx-permissions/ngx-permissions.umd.js");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var MagazineListComponent = /** @class */ (function () {
    function MagazineListComponent(magazineServiceFactory, permissionsService) {
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this._magazineService = magazineServiceFactory;
        this._permissionsService = permissionsService;
    }
    MagazineListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var permission = authorization_service_1.AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);
        this.view = this._magazineService.pipe(map_1.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); }));
        this._magazineService.read();
    };
    MagazineListComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this._magazineService.read();
    };
    MagazineListComponent.prototype.addHandler = function (_a, formInstance) {
        var sender = _a.sender;
        formInstance.reset();
        this.closeEditor(sender);
        sender.addRow(new magazine_model_1.MagazineModel());
    };
    MagazineListComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this._editedRowIndex = rowIndex;
        this._editedProduct = Object.assign({}, dataItem);
        sender.editRow(rowIndex);
    };
    MagazineListComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    MagazineListComponent.prototype.saveHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem, isNew = _a.isNew;
        this._magazineService.save(dataItem, isNew);
        sender.closeRow(rowIndex);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    MagazineListComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this._magazineService.remove(dataItem);
    };
    MagazineListComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this._editedRowIndex; }
        grid.closeRow(rowIndex);
        this._magazineService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    MagazineListComponent = __decorate([
        core_1.Component({
            selector: 'magazine-list',
            template: __webpack_require__("../../../../../src/app/components/magazine/magazine-list/magazine-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/magazine/magazine-list/magazine-list.component.css")]
        }),
        __param(0, core_1.Inject(magazine_service_1.MagazineService)),
        __metadata("design:paramtypes", [Object, ngx_permissions_1.NgxPermissionsService])
    ], MagazineListComponent);
    return MagazineListComponent;
}());
exports.MagazineListComponent = MagazineListComponent;


/***/ }),

/***/ "../../../../../src/app/components/publication-house/publication-house-list/publication-house-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/publication-house/publication-house-list/publication-house-list.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<h2>Publication Houses</h2>\r\n\r\n<div *ngxPermissionsOnly=\"['user']\"> user mode</div>\r\n<div *ngxPermissionsOnly=\"['admin']\"> admin mode</div>\r\n\r\n<form novalidate #myForm=\"ngForm\">\r\n    <kendo-grid [data]=\"view | async\"\r\n                [height]=\"550\"\r\n                (dataStateChange)=\"onStateChange($event)\"\r\n                (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\r\n                (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\r\n                [navigable]=\"true\"\r\n                (add)=\"addHandler($event, myForm)\">\r\n        <ng-template kendoGridToolbarTemplate *ngxPermissionsOnly=\"['admin']\">\r\n            <button kendoGridAddCommand type=\"button\" class=\"btn\">Add new</button>\r\n        </ng-template>\r\n        <kendo-grid-column field=\"id\" title=\"ID\" width=\"80\" editable=\"false\" nullable=\"false\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.id\" kendoGridFocusable name=\"id\" class=\"k-textbox\" hidden />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"name\" title=\"Name\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.name\" kendoGridFocusable name=\"name\" class=\"k-textbox\" placeholder=\"Name\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"address\" title=\"Address\" width=\"200\">\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <input [(ngModel)]=\"dataItem.address\" kendoGridFocusable name=\"address\" class=\"k-textbox\" placeholder=\"Address\" />\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"booksIn\" title=\"Books in Publishing\" width=\"200\">\r\n            <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n                <p *ngFor=\"let b of dataItem.bookList\">{{b.title}}</p>\r\n            </ng-template>\r\n            <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n                <kendo-multiselect [data]=\"allBookList\"\r\n                                   [(ngModel)]=\"selectedBookList\"\r\n                                   [textField]=\"'title'\"\r\n                                   [valueField]=\"'id'\"\r\n                                   [placeholder]=\"'Select some...'\"\r\n                                   [ngModelOptions]=\"{standalone: true}\">\r\n                </kendo-multiselect>\r\n            </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-command-column *ngxPermissionsOnly=\"['admin']\" title=\" \" width=\"220\">\r\n            <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n                <button kendoGridEditCommand type=\"button\" [primary]=\"true\">Edit</button>\r\n                <button kendoGridRemoveCommand type=\"button\">Remove</button>\r\n                <button kendoGridSaveCommand type=\"button\" [disabled]=\"myForm.invalid\"> {{ isNew ? 'Add' : 'Update' }} </button>\r\n                <button kendoGridCancelCommand type=\"button\">{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n            </ng-template>\r\n        </kendo-grid-command-column>\r\n    </kendo-grid>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/publication-house/publication-house-list/publication-house-list.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var publication_house_service_1 = __webpack_require__("../../../../../src/app/services/publication-house.service.ts");
var publication_house_request_model_1 = __webpack_require__("../../../../../src/app/models/publication-house-request.model.ts");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var ngx_permissions_1 = __webpack_require__("../../../../ngx-permissions/ngx-permissions.umd.js");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var PublicationHouseListComponent = /** @class */ (function () {
    function PublicationHouseListComponent(publicationHouseServiceFactory, permissionsService) {
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.allBookList = new Array();
        this.selectedBookList = new Array();
        this._publicationHouseService = publicationHouseServiceFactory;
        this._permissionsService = permissionsService;
    }
    PublicationHouseListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var permission = authorization_service_1.AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);
        this.view = this._publicationHouseService.pipe(map_1.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); }));
        this._publicationHouseService.read();
    };
    PublicationHouseListComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this._publicationHouseService.read();
    };
    PublicationHouseListComponent.prototype.addHandler = function (_a, formInstance) {
        var _this = this;
        var sender = _a.sender;
        formInstance.reset();
        this.closeEditor(sender);
        this._publicationHouseService.loadAllBooks().subscribe(function (data) { return _this.allBookList = data; });
        this.selectedBookList = [];
        sender.addRow(new publication_house_request_model_1.PublicationHouseRequestModel());
    };
    PublicationHouseListComponent.prototype.editHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this._editedProduct = Object.assign({}, dataItem);
        this._publicationHouseService.loadAllBooks().subscribe(function (data) { return _this.allBookList = data; });
        this._publicationHouseService.loadBooksId(this._editedProduct.id).subscribe(function (data) {
            _this.selectedBookList = data;
        });
        sender.editRow(rowIndex);
    };
    PublicationHouseListComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    PublicationHouseListComponent.prototype.saveHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem, isNew = _a.isNew;
        this._publicationHouseService.save(dataItem, this.selectedBookList, isNew);
        sender.closeRow(rowIndex);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    PublicationHouseListComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this._publicationHouseService.remove(dataItem);
    };
    PublicationHouseListComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this._editedRowIndex; }
        grid.closeRow(rowIndex);
        this._publicationHouseService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    };
    PublicationHouseListComponent = __decorate([
        core_1.Component({
            selector: 'publication-house-list',
            template: __webpack_require__("../../../../../src/app/components/publication-house/publication-house-list/publication-house-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/publication-house/publication-house-list/publication-house-list.component.css")]
        }),
        __param(0, core_1.Inject(publication_house_service_1.PublicationHouseService)),
        __metadata("design:paramtypes", [Object, ngx_permissions_1.NgxPermissionsService])
    ], PublicationHouseListComponent);
    return PublicationHouseListComponent;
}());
exports.PublicationHouseListComponent = PublicationHouseListComponent;


/***/ }),

/***/ "../../../../../src/app/models/book-request.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var BookRequestModel = /** @class */ (function () {
    function BookRequestModel() {
        this.publicationHouseIdList = new Array();
    }
    return BookRequestModel;
}());
exports.BookRequestModel = BookRequestModel;


/***/ }),

/***/ "../../../../../src/app/models/brochure.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var BrochureResponseModel = /** @class */ (function () {
    function BrochureResponseModel() {
        this.brochureList = new Array();
    }
    return BrochureResponseModel;
}());
exports.BrochureResponseModel = BrochureResponseModel;
var BrochureItem = /** @class */ (function () {
    function BrochureItem() {
    }
    return BrochureItem;
}());
exports.BrochureItem = BrochureItem;


/***/ }),

/***/ "../../../../../src/app/models/magazine.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var MagazineModel = /** @class */ (function () {
    function MagazineModel() {
    }
    return MagazineModel;
}());
exports.MagazineModel = MagazineModel;
var MagazineResponseModel = /** @class */ (function () {
    function MagazineResponseModel() {
        this.magazineList = new Array();
    }
    return MagazineResponseModel;
}());
exports.MagazineResponseModel = MagazineResponseModel;


/***/ }),

/***/ "../../../../../src/app/models/publication-house-request.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var PublicationHouseRequestModel = /** @class */ (function () {
    function PublicationHouseRequestModel() {
        this.bookIdList = new Array();
    }
    return PublicationHouseRequestModel;
}());
exports.PublicationHouseRequestModel = PublicationHouseRequestModel;


/***/ }),

/***/ "../../../../../src/app/services/authorization.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var angular2_jwt_1 = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var AuthorizationService = /** @class */ (function () {
    function AuthorizationService(http) {
        this._authorizationStatusSource = new BehaviorSubject_1.BehaviorSubject(false);
        this._http = http;
    }
    AuthorizationService.prototype.regiser = function (data) {
        return this._http.post('api/Authorization/Register', data)
            .pipe(map_1.map(function (x) {
            return x;
        }));
    };
    AuthorizationService.prototype.login = function (data) {
        var _this = this;
        var authorizationToken;
        var permission;
        return this._http.post('api/Authorization/Login', data)
            .map(function (response) {
            authorizationToken = response.authorizationToken;
            permission = response.permission;
            localStorage.setItem('authorizationToken', authorizationToken);
            localStorage.setItem('permission', permission);
            _this._authorizationStatusSource.next(true);
        });
    };
    AuthorizationService.prototype.logOut = function () {
        var response = this._http.post('api/Authorization/LogOut', '').subscribe(function (message) { return console.log(message); });
        localStorage.clear();
        return response;
    };
    AuthorizationService.prototype.regiserAsAdmin = function (data) {
        return this._http.post('api/Authorization/AddAdmin', data)
            .pipe(map_1.map(function (x) {
            return x;
        }));
    };
    AuthorizationService.getPermission = function () {
        var permissions = new Array();
        var isAuthentificated = this.isAuthentificated();
        if (isAuthentificated) {
            var loadedPermission = localStorage.getItem('permission');
            permissions.push(loadedPermission);
        }
        return permissions;
    };
    AuthorizationService.getToken = function () {
        return localStorage.getItem('authorizationToken');
    };
    AuthorizationService.isAuthentificated = function () {
        return angular2_jwt_1.tokenNotExpired(null, this.getToken());
    };
    AuthorizationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], AuthorizationService);
    return AuthorizationService;
}());
exports.AuthorizationService = AuthorizationService;


/***/ }),

/***/ "../../../../../src/app/services/books.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var tap_1 = __webpack_require__("../../../../rxjs/_esm5/operators/tap.js");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var book_request_model_1 = __webpack_require__("../../../../../src/app/models/book-request.model.ts");
var CREATE_ACTION = 'Create';
var UPDATE_ACTION = 'Update';
var REMOVE_ACTION = 'Destroy';
var BooksService = /** @class */ (function (_super) {
    __extends(BooksService, _super);
    function BooksService(http) {
        var _this = _super.call(this, []) || this;
        _this.data = Array();
        _this._http = http;
        return _this;
    }
    BooksService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.fetch()
            .pipe(tap_1.tap(function (data) {
            _this.data = data;
        }))
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    BooksService.prototype.save = function (data, selectedPublicationHouseList, isNew) {
        var _this = this;
        var action = isNew ? CREATE_ACTION : UPDATE_ACTION;
        this.reset();
        data.selectedPublicationHouseList = selectedPublicationHouseList;
        this.fetch(action, data).subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    BooksService.prototype.remove = function (data) {
        var _this = this;
        this.reset();
        this.fetch(REMOVE_ACTION, data).subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    BooksService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.id === dataItem.id; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    BooksService.prototype.reset = function () {
        this.data = new Array();
    };
    BooksService.prototype.fetch = function (action, data) {
        if (action === void 0) { action = ''; }
        if (action == '') {
            var result = this._http.get('/api/Book')
                .map(function (response) {
                return response.bookList;
            });
            return result;
        }
        if (action == 'Create') {
            var book = new book_request_model_1.BookRequestModel();
            book.authorName = data.authorName;
            book.title = data.title;
            book.yearOfPublish = data.yearOfPublish;
            if (data.selectedPublicationHouseList) {
                for (var i = 0; i < data.selectedPublicationHouseList.length; i++) {
                    book.publicationHouseIdList[i] = data.selectedPublicationHouseList[i].id;
                }
            }
            return this._http.post('/api/Book', book).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Update') {
            var book = new book_request_model_1.BookRequestModel();
            book.id = data.id;
            book.authorName = data.authorName;
            book.title = data.title;
            book.yearOfPublish = data.yearOfPublish;
            if (data.selectedPublicationHouseList) {
                for (var i = 0; i < data.selectedPublicationHouseList.length; i++) {
                    book.publicationHouseIdList[i] = data.selectedPublicationHouseList[i].id;
                }
            }
            return this._http.put('/api/Book/' + book.id, book).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Destroy') {
            var book = data;
            return this._http.delete('/api/Book/' + book.id).pipe(map_1.map(function (response) { return response; }));
        }
        return null;
    };
    BooksService.prototype.loadAllPublicationHouses = function () {
        var result = this._http.get('/api/GetSimplePublicationHouses')
            .map(function (response) {
            return response.publicationHouseList;
        });
        return result;
    };
    BooksService.prototype.loadPublicationHouseId = function (id) {
        var result = this._http.get('/api/GetPublicationHouseForBook/' + id)
            .map(function (response) {
            return response.publicationHouseList;
        });
        return result;
    };
    BooksService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], BooksService);
    return BooksService;
}(BehaviorSubject_1.BehaviorSubject));
exports.BooksService = BooksService;


/***/ }),

/***/ "../../../../../src/app/services/brochure.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var tap_1 = __webpack_require__("../../../../rxjs/_esm5/operators/tap.js");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var CREATE_ACTION = 'Create';
var UPDATE_ACTION = 'Update';
var REMOVE_ACTION = 'Destroy';
var BrochureService = /** @class */ (function (_super) {
    __extends(BrochureService, _super);
    function BrochureService(http) {
        var _this = _super.call(this, []) || this;
        _this.data = Array();
        _this._http = http;
        return _this;
    }
    BrochureService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.fetch()
            .pipe(tap_1.tap(function (data) {
            _this.data = data;
        }))
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    BrochureService.prototype.save = function (data, isNew) {
        var _this = this;
        var action = isNew ? CREATE_ACTION : UPDATE_ACTION;
        this.reset();
        this.fetch(action, data).subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    BrochureService.prototype.remove = function (data) {
        var _this = this;
        this.reset();
        this.fetch(REMOVE_ACTION, data)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    BrochureService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.id === dataItem.id; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    BrochureService.prototype.reset = function () {
        this.data = new Array();
    };
    BrochureService.prototype.fetch = function (action, data) {
        if (action === void 0) { action = ''; }
        if (action == '') {
            var result = this._http.get('/api/Brochure')
                .map(function (response) {
                return response.brochureList;
            });
            return result;
        }
        if (action == 'Create') {
            var brochure = data;
            brochure.id = 0;
            return this._http.post('/api/Brochure', brochure).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Update') {
            var brochure = data;
            return this._http.put('/api/Brochure/' + brochure.id, brochure).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Destroy') {
            var brochure = data;
            return this._http.delete('/api/Brochure/' + brochure.id).pipe(map_1.map(function (response) { return response; }));
        }
    };
    BrochureService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], BrochureService);
    return BrochureService;
}(BehaviorSubject_1.BehaviorSubject));
exports.BrochureService = BrochureService;


/***/ }),

/***/ "../../../../../src/app/services/magazine.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var tap_1 = __webpack_require__("../../../../rxjs/_esm5/operators/tap.js");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var CREATE_ACTION = 'Create';
var UPDATE_ACTION = 'Update';
var REMOVE_ACTION = 'Destroy';
var MagazineService = /** @class */ (function (_super) {
    __extends(MagazineService, _super);
    function MagazineService(http) {
        var _this = _super.call(this, []) || this;
        _this.data = new Array();
        _this._http = http;
        return _this;
    }
    MagazineService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.fetch()
            .pipe(tap_1.tap(function (data) {
            _this.data = data;
        }))
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    MagazineService.prototype.save = function (data, isNew) {
        var _this = this;
        var action = isNew ? CREATE_ACTION : UPDATE_ACTION;
        this.reset();
        this.fetch(action, data)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    MagazineService.prototype.remove = function (data) {
        var _this = this;
        this.reset();
        this.fetch(REMOVE_ACTION, data)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    MagazineService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.id === dataItem.id; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    MagazineService.prototype.reset = function () {
        this.data = new Array();
    };
    MagazineService.prototype.fetch = function (action, data) {
        if (action === void 0) { action = ''; }
        if (action == '') {
            var result = this._http.get('/api/Magazine')
                .map(function (response) {
                return response.magazineList;
            });
            return result;
        }
        if (action == 'Create') {
            var magazine = data;
            magazine.id = 0;
            return this._http.post('/api/Magazine', magazine).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Update') {
            var magazine = data;
            return this._http.put('/api/Magazine/' + magazine.id, data).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Destroy') {
            var magazine = data;
            return this._http.delete('/api/Magazine/' + magazine.id).pipe(map_1.map(function (response) { return response; }));
        }
    };
    MagazineService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], MagazineService);
    return MagazineService;
}(BehaviorSubject_1.BehaviorSubject));
exports.MagazineService = MagazineService;


/***/ }),

/***/ "../../../../../src/app/services/publication-house.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var tap_1 = __webpack_require__("../../../../rxjs/_esm5/operators/tap.js");
var map_1 = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
var publication_house_request_model_1 = __webpack_require__("../../../../../src/app/models/publication-house-request.model.ts");
var CREATE_ACTION = 'Create';
var UPDATE_ACTION = 'Update';
var REMOVE_ACTION = 'Destroy';
var PublicationHouseService = /** @class */ (function (_super) {
    __extends(PublicationHouseService, _super);
    function PublicationHouseService(http) {
        var _this = _super.call(this, []) || this;
        _this.data = new Array();
        _this._http = http;
        return _this;
    }
    PublicationHouseService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.fetch()
            .pipe(tap_1.tap(function (data) {
            _this.data = data;
        }))
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    PublicationHouseService.prototype.save = function (data, selectedBookList, isNew) {
        var _this = this;
        var action = isNew ? CREATE_ACTION : UPDATE_ACTION;
        this.reset();
        data.selectedBookList = selectedBookList;
        this.fetch(action, data)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    PublicationHouseService.prototype.remove = function (data) {
        var _this = this;
        this.reset();
        this.fetch(REMOVE_ACTION, data)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    PublicationHouseService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.id === dataItem.id; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    PublicationHouseService.prototype.reset = function () {
        this.data = new Array();
    };
    PublicationHouseService.prototype.fetch = function (action, data) {
        if (action === void 0) { action = ''; }
        if (action == '') {
            var result = this._http.get('/api/PublicationHouse')
                .map(function (response) {
                return response.publicationHouseList;
            });
            return result;
        }
        if (action == 'Create') {
            var publicationHouse = new publication_house_request_model_1.PublicationHouseRequestModel();
            publicationHouse.name = data.name;
            publicationHouse.address = data.address;
            for (var i = 0; i < data.selectedBookList.length; i++) {
                publicationHouse.bookIdList[i] = data.selectedBookList[i].id;
            }
            return this._http.post('/api/PublicationHouse', publicationHouse).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Update') {
            var publicationHouse = new publication_house_request_model_1.PublicationHouseRequestModel();
            publicationHouse.id = data.id;
            publicationHouse.name = data.name;
            publicationHouse.address = data.address;
            for (var i = 0; i < data.selectedBookList.length; i++) {
                publicationHouse.bookIdList[i] = data.selectedBookList[i].id;
            }
            return this._http.put('/api/PublicationHouse/' + publicationHouse.id, publicationHouse).pipe(map_1.map(function (response) { return response; }));
        }
        if (action == 'Destroy') {
            var publicationHouse = data;
            return this._http.delete('/api/PublicationHouse/' + publicationHouse.id).pipe(map_1.map(function (response) { return response; }));
        }
    };
    PublicationHouseService.prototype.loadAllBooks = function () {
        var result = this._http.get('/api/GetSimpleBooks')
            .map(function (response) {
            return response.bookList;
        });
        return result;
    };
    PublicationHouseService.prototype.loadBooksId = function (id) {
        var result = this._http.get('/api/GetBookForPublicationHouse/' + id)
            .map(function (response) {
            return response.bookList;
        });
        return result;
    };
    PublicationHouseService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], PublicationHouseService);
    return PublicationHouseService;
}(BehaviorSubject_1.BehaviorSubject));
exports.PublicationHouseService = PublicationHouseService;


/***/ }),

/***/ "../../../../../src/app/shared/brochure-type-of-cover.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TypeOfCover;
(function (TypeOfCover) {
    TypeOfCover[TypeOfCover["Hard"] = 0] = "Hard";
    TypeOfCover[TypeOfCover["Soft"] = 1] = "Soft";
})(TypeOfCover = exports.TypeOfCover || (exports.TypeOfCover = {}));


/***/ }),

/***/ "../../../../../src/app/shared/nav-menu/nav-menu.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "li .glyphicon {\r\n    margin-right: 10px;\r\n}\r\n\r\n/* Highlighting rules for nav menu items */\r\n\r\nli.link-active a,\r\nli.link-active a:hover,\r\nli.link-active a:focus {\r\n    background-color: #4189C7;\r\n    color: white;\r\n}\r\n\r\n/* Keep the nav menu independent of scrolling and on top of other items */\r\n\r\n.main-nav {\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    right: 0;\r\n    z-index: 1;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n    /* On small screens, convert the nav menu to a vertical sidebar */\r\n    .main-nav {\r\n        height: 100%;\r\n        width: calc(25% - 20px);\r\n    }\r\n    .navbar {\r\n        border-radius: 0px;\r\n        border-width: 0px;\r\n        height: 100%;\r\n    }\r\n    .navbar-header {\r\n        float: none;\r\n    }\r\n    .navbar-collapse {\r\n        border-top: 1px solid #444;\r\n        padding: 0px;\r\n    }\r\n    .navbar ul {\r\n        float: none;\r\n    }\r\n    .navbar li {\r\n        float: none;\r\n        font-size: 15px;\r\n        margin: 6px;\r\n    }\r\n    .navbar li a {\r\n        padding: 10px 16px;\r\n        border-radius: 4px;\r\n    }\r\n    .navbar a {\r\n        /* If a menu item's text is too long, truncate it */\r\n        width: 100%;\r\n        white-space: nowrap;\r\n        overflow: hidden;\r\n        text-overflow: ellipsis;\r\n    }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/nav-menu/nav-menu.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div *ngxPermissionsOnly=\"['admin','user']\">\r\n  <nav class=\"navbar navbar-light bg-faded\">\r\n    <a class=\"navbar-item\" [routerLink]=\"['']\">Library</a>\r\n    <ul class=\"nav navbar-nav\">\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" [routerLink]=\"['']\">Books</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" [routerLink]=\"['magazine']\">Magazines</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" [routerLink]=\"['brochure']\">Brochures</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" [routerLink]=\"['publicationHouse']\">Publication Houses</a>\r\n      </li>\r\n      <li *ngxPermissionsExcept=\"['admin','user']\" class=\"nav-item\">\r\n        <a class=\"nav-link\" [routerLink]=\"['authorization/login']\">Login</a>\r\n      </li>\r\n      <li *ngxPermissionsOnly=\"['admin','user']\" class=\"nav-item\">\r\n        <a class=\"nav-link\" (click)=\"logOut()\">Log Out</a>\r\n      </li>\r\n    </ul>\r\n  </nav>\r\n</div>\r\n\r\n<div *ngxPermissionsExcept=\"['admin','user']\">\r\n  <h2> Login to see some more</h2>\r\n  <nav class=\"navbar navbar-light bg-faded\">\r\n    <a class=\"navbar-item\" [routerLink]=\"['']\">Library</a>\r\n    <ul class=\"nav navbar-nav\">\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" [routerLink]=\"['authorization/login']\">Login</a>\r\n      </li>\r\n    </ul>\r\n  </nav>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/nav-menu/nav-menu.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var ngx_permissions_1 = __webpack_require__("../../../../ngx-permissions/ngx-permissions.umd.js");
var NavMenuComponent = /** @class */ (function () {
    function NavMenuComponent(permissionsService, service) {
        this._permissionsService = permissionsService;
        this._authorizationService = service;
    }
    NavMenuComponent.prototype.ngOnInit = function () {
        this._permissionsService.loadPermissions(authorization_service_1.AuthorizationService.getPermission());
    };
    NavMenuComponent.prototype.logOut = function () {
        this._authorizationService.logOut();
        this.ngOnInit();
    };
    NavMenuComponent = __decorate([
        core_1.Component({
            selector: 'app-nav-menu',
            template: __webpack_require__("../../../../../src/app/shared/nav-menu/nav-menu.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/nav-menu/nav-menu.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_permissions_1.NgxPermissionsService,
            authorization_service_1.AuthorizationService])
    ], NavMenuComponent);
    return NavMenuComponent;
}());
exports.NavMenuComponent = NavMenuComponent;


/***/ }),

/***/ "../../../../../src/app/token.interceptor.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
__webpack_require__("../../../../rxjs/_esm5/add/operator/do.js");
var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(router) {
        this._router = router;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        request = request.clone({
            setHeaders: {
                Authorization: "Bearer " + authorization_service_1.AuthorizationService.getToken()
            }
        });
        return next.handle(request).do(function (event) {
            if (event instanceof http_1.HttpResponse) {
            }
        }, function (err) {
            if (err instanceof http_1.HttpErrorResponse) {
                if (err.status === 401) {
                    _this._router.navigate(['/authorization/login']);
                }
            }
        });
    };
    TokenInterceptor = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], TokenInterceptor);
    return TokenInterceptor;
}());
exports.TokenInterceptor = TokenInterceptor;


/***/ }),

/***/ "../../../../../src/app/window.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var WINDOW = new core_1.InjectionToken('window');
exports.windowProvider = { provide: WINDOW, useValue: window };


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map