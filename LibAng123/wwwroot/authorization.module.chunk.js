webpackJsonp(["authorization.module"],{

/***/ "../../../../../src/app/components/authorization/authorization-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var register_component_1 = __webpack_require__("../../../../../src/app/components/authorization/register/register.component.ts");
var login_component_1 = __webpack_require__("../../../../../src/app/components/authorization/login/login.component.ts");
var routes = [
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'register', component: register_component_1.RegisterComponent
    }
];
var AuthorizationRoutingModule = /** @class */ (function () {
    function AuthorizationRoutingModule() {
    }
    AuthorizationRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], AuthorizationRoutingModule);
    return AuthorizationRoutingModule;
}());
exports.AuthorizationRoutingModule = AuthorizationRoutingModule;


/***/ }),

/***/ "../../../../../src/app/components/authorization/authorization.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var register_component_1 = __webpack_require__("../../../../../src/app/components/authorization/register/register.component.ts");
var login_component_1 = __webpack_require__("../../../../../src/app/components/authorization/login/login.component.ts");
var authorization_routing_module_1 = __webpack_require__("../../../../../src/app/components/authorization/authorization-routing.module.ts");
var AuthorizationModule = /** @class */ (function () {
    function AuthorizationModule() {
    }
    AuthorizationModule = __decorate([
        core_1.NgModule({
            imports: [
                forms_1.FormsModule,
                common_1.CommonModule,
                authorization_routing_module_1.AuthorizationRoutingModule
            ],
            declarations: [
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent
            ]
        })
    ], AuthorizationModule);
    return AuthorizationModule;
}());
exports.AuthorizationModule = AuthorizationModule;


/***/ }),

/***/ "../../../../../src/app/components/authorization/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/authorization/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-xs-10\" >\r\n  <div class=\"form-group\">\r\n    <label>Login</label>\r\n    <input [(ngModel)]=\"user.name\"\r\n           placeholder=\"Name\"\r\n           type=\"text\" class=\"form-control\"\r\n           #name=\"ngModel\" required>\r\n    <div [hidden]=\"name.valid\" class=\"alert alert-danger\">\r\n      Name required!\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>Password</label>\r\n    <input [(ngModel)]=\"user.password\"\r\n           placeholder=\"Password\"\r\n           type=\"password\"\r\n           class=\"form-control\"\r\n           #password=\"ngModel\" required>\r\n    <div [hidden]=\"password.valid\" class=\"alert alert-danger\">\r\n      Password required!\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <button [disabled]=\"name.invalid || password.invalid\"\r\n            class=\"btn btn-default\"\r\n            (click)=\"login()\">\r\n      Login\r\n    </button>\r\n  </div>\r\n  <button class=\"btn btn-default\"\r\n          (click)=\"toRegister()\">\r\n    Go to registration\r\n  </button>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/authorization/login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var user_view_model_1 = __webpack_require__("../../../../../src/app/models/user-view.model.ts");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authorizationServiceFactory, router) {
        this.authorizationServiceFactory = authorizationServiceFactory;
        this._authorizationService = authorizationServiceFactory;
        this._router = router;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.user = new user_view_model_1.UserViewModel();
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this._authorizationService.login(this.user).subscribe(function (data) {
            _this._router.navigate(['/']);
        });
    };
    LoginComponent.prototype.toRegister = function () {
        this._router.navigate(['/authorization/register']);
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/authorization/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/authorization/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [authorization_service_1.AuthorizationService, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "../../../../../src/app/components/authorization/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/authorization/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-xs-10\">\r\n  <div class=\"form-group\">\r\n    <label>Login</label>\r\n    <input [(ngModel)]=\"user.name\"\r\n            placeholder=\"Name\"\r\n            type=\"text\" class=\"form-control\"\r\n            #name=\"ngModel\" required>\r\n    <div [hidden]=\"name.valid\" class=\"alert alert-danger\">\r\n      Name required!\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>Password</label>\r\n    <input [(ngModel)]=\"user.password\"\r\n            placeholder=\"Password\"\r\n            type=\"password\"\r\n            class=\"form-control\"\r\n            #password=\"ngModel\" required>\r\n    <div [hidden]=\"password.valid\" class=\"alert alert-danger\">\r\n      Password required!\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>EMail</label>\r\n    <input [(ngModel)]=\"user.email\" placeholder=\"EMail\" type=\"email\" class=\"form-control\">\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <div class=\"row\">\r\n       Are you Admin?\r\n      <input type=\"checkbox\" [(ngModel)]=\"adminCheckbox\" />\r\n    </div>\r\n    <button [disabled]=\"name.invalid || password.invalid\"\r\n            class=\"btn btn-default\"\r\n            (click)=\"register()\">\r\n      Register\r\n    </button>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/authorization/register/register.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var authorization_service_1 = __webpack_require__("../../../../../src/app/services/authorization.service.ts");
var user_view_model_1 = __webpack_require__("../../../../../src/app/models/user-view.model.ts");
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(AuthorizationServiceFactory, router) {
        this.adminCheckbox = false;
        this._authorizationService = AuthorizationServiceFactory;
        this._router = router;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.user = new user_view_model_1.UserViewModel();
    };
    RegisterComponent.prototype.register = function () {
        if (this.adminCheckbox) {
            this._authorizationService.regiserAsAdmin(this.user).subscribe(function (error) { return console.log(error); });
            this._router.navigateByUrl("/");
            return;
        }
        this._authorizationService.regiser(this.user).subscribe(function (error) { return console.log(error); });
        this._router.navigateByUrl("/");
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/authorization/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/authorization/register/register.component.css")]
        }),
        __param(0, core_1.Inject(authorization_service_1.AuthorizationService)),
        __metadata("design:paramtypes", [Object, router_1.Router])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;


/***/ }),

/***/ "../../../../../src/app/models/user-view.model.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var UserViewModel = /** @class */ (function () {
    function UserViewModel() {
    }
    return UserViewModel;
}());
exports.UserViewModel = UserViewModel;


/***/ })

});
//# sourceMappingURL=authorization.module.chunk.js.map