namespace AngularLibrary.Helpers
{
    public class LogStrings
    {
        public const string GetNotFoundMessage = "Not found while GET",
                            GetCriticalMessage = "Something crashed while GET",
                            GetOKMessage = "All fine while GET",

                            UpdateNotFoundMessage = "Not found while UPDATE",
                            UpdateCriticalMessage = "Something crashed while UPDATE",
                            UpdateOKMessage = "All fine while UPDATE",

                            CreateNotFoundMessage = "Not found while CREATE",
                            CreateCriticalMessage = "Something crashed while CREATE",
                            CreateOKMessage = "All fine while CREATE",

                            DeleteNotFoundMessage = "Not found while DELETE",
                            DeleteCriticalMessage = "Something crashed while DELETE",
                            DeleteOKMessage = "All fine while DELETE",

                            RegisterErrorMessage = "Register Error",
                            LoginErrorMessage = "Login Error";
    }
}
