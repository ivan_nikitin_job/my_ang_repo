using AngularLibrary.BusinessLogicLayer.Exceptions.BookExceptions;
using AngularLibrary.BusinessLogicLayer.Exceptions.BookPublicationHouseExceptions;
using AngularLibrary.BusinessLogicLayer.Exceptions.PublicationHouseExceptions;
using AngularLibrary.Helpers;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.BookViewModels;
using AngularLibrary.ViewModels.PublicationHouseViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AngularLibrary.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize]
    public class PublicationHouseController : Controller
    {
        private IPublicationHouseService _publicationHouseService;
        private IBookService _bookServices;
        private ILogger _logger;

        public PublicationHouseController(IPublicationHouseService publicationHouseService, IBookService bookService, ILoggerFactory logger)
        {
            _publicationHouseService = publicationHouseService;
            _bookServices = bookService;
            _logger = logger.CreateLogger("PublicationHouse logger");
        }

        [HttpGet]
        public async Task<IActionResult> GetPublicationHouses()
        {
            try
            {
                var publicationHouseList = new GetComplexPublicationHouseViewModel();

                publicationHouseList = await _publicationHouseService.GetComplexPublicationHouseList();

                return Ok(publicationHouseList);
            }

            catch (BookListErrorException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (GetPublicationHouseListException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (GetRelationsException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);
                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpGet("/api/GetSimplePublicationHouses")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public async Task<IActionResult> GetSimplePublicationHouses()
        {
            try
            {
                var publicationHouseList = new GetSimplePublicationHouseViewModel();
                publicationHouseList = await _publicationHouseService.GetSimplePublicationHouseList();

                return Ok(publicationHouseList);
            }

            catch (GetPublicationHouseListException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpGet("/api/GetBookForPublicationHouse/{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public async Task<IActionResult> GetBookForPublicationHouse([FromRoute] long id)
        {
            try
            {
                var bookViewModel = new GetSimpleBookViewModel();

                bookViewModel = await _bookServices.GetBookListModelByPublicationHouseID(id);

                return Ok(bookViewModel);
            }

            catch (GetRelationsByPublicationHouseIDException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult UpdatePublicationHouse([FromBody] UpdatePublicationHouseRequestModel publicationHouseViewModel)
        {
            try
            {
                _publicationHouseService.UpdatePublicationHouse(publicationHouseViewModel);

                return Ok();
            }

            catch (UpdatePublicationHouseException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.UpdateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.UpdateCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPost]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult CreatePublicationHouse([FromBody] CreatePublicationHouseRequestModel publicationHouseViewModel)
        {
            try
            {
                _publicationHouseService.CreatePublicationHouse(publicationHouseViewModel);

                return Ok();
            }

            catch (CreatePublicationHouseException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.CreateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.CreateCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult DeletePublicationHouse([FromRoute] long id)
        {
            try
            {
                _publicationHouseService.DeletePublicationHouse(id);

                return Ok();
            }

            catch (DeletePublicationHouseException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.DeleteNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.DeleteCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }
    }
}
