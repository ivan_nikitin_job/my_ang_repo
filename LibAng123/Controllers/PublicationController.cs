using AngularLibrary.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace AngularLibrary.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class PublicationController : Controller
    {
        private PublicationService _service;

        public PublicationController(IConfiguration configuration, IMapper mapper)
        {
            _service = new PublicationService(configuration, mapper);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> AllPublications()
        {
            var publicationList = await _service.GetAll();

            return Ok(publicationList);
        }
    }
}
