using AngularLibrary.BusinessLogicLayer.Exceptions.MagazineExceptions;
using AngularLibrary.Helpers;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.MagazineViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AngularLibrary.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize]
    public class MagazineController : Controller
    {
        private IMagazineService _magazineService;
        private ILogger _logger;

        public MagazineController(IMagazineService magazineService, ILoggerFactory logger)
        {
            _magazineService = magazineService;
            _logger = logger.CreateLogger("Magazine logger");
        }

        [HttpGet]
        public async Task<IActionResult> GetMagazines()
        {
            try
            {
                var magazineList = new GetMagazineViewModel();

                magazineList = await _magazineService.GetMagazineList();

                return Ok(magazineList);
            }

            catch (GetMagazineListException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult UpdateMagazine([FromBody] UpdateMagazineRequestModel magazine)
        {
            try
            {
                _magazineService.UpdateMagazine(magazine);

                return Ok();
            }

            catch (UpdateMagazineException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.UpdateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.UpdateCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPost]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult CreateMagazine([FromBody] CreateMagazineRequestModel magazine)
        {
            try
            {
                _magazineService.CreateMagazine(magazine);

                return Ok();
            }

            catch (CreateMagazineException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.CreateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.RegisterErrorMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult DeleteMagazine([FromRoute] long id)
        {
            try
            {
                _magazineService.DeleteMagazine(id);

                return Ok();
            }

            catch (DeleteMagazineException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.DeleteNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.DeleteCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }
    }
}
