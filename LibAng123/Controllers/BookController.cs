using AngularLibrary.BusinessLogicLayer.Exceptions.BookExceptions;
using AngularLibrary.BusinessLogicLayer.Exceptions.BookPublicationHouseExceptions;
using AngularLibrary.BusinessLogicLayer.Exceptions.PublicationHouseExceptions;
using AngularLibrary.Helpers;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.BookViewModels;
using AngularLibrary.ViewModels.PublicationHouseViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AngularLibrary.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize]
    public class BookController : Controller
    {
        private readonly IBookService _bookService;
        private readonly IPublicationHouseService _publicationHouseServices;

        private ILogger _logger;

        public BookController(IBookService bookService, IPublicationHouseService publicationHouseService, ILoggerProvider loggerProvider)
        {
            _bookService = bookService;
            _publicationHouseServices = publicationHouseService;
            _logger = loggerProvider.CreateLogger("Book");
        }

        [HttpGet]
        public async Task<IActionResult> Book()
        {
            try
            {
                var bookViewModel = new GetComplexBookViewModel();

                bookViewModel = await _bookService.GetComplexBookList();

                return Ok(bookViewModel);
            }

            catch (BookListErrorException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (GetPublicationHouseListException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (GetRelationsException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult UpdateBook([FromBody] UpdateBookRequestModel book)
        {
            try
            {
                _bookService.UpdateBook(book);

                return Ok();
            }

            catch (BookUpdateException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.UpdateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.UpdateNotFoundMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpGet("/api/GetPublicationHouseForBook/{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public async Task<IActionResult> GetPublicationHouseForBook([FromRoute] long id)
        {
            try
            {
                var publicationHouseList = new GetSimplePublicationHouseViewModel();

                publicationHouseList = await _publicationHouseServices.GetPublicationHouseListModelByBookID(id);

                return Ok(publicationHouseList);
            }

            catch (GetRelationsByBookIDException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (GetPublicationHouseListException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpGet("/api/GetSimpleBooks")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public async Task<IActionResult> GetSimpleBooks()
        {
            try
            {
                var bookList = new GetSimpleBookViewModel();

                bookList = await _bookService.GetSimpleBookList();

                return Ok(bookList);
            }

            catch (BookListErrorException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPost]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult CreateBook([FromBody] CreateBookRequestModel bookViewModel)
        {
            try
            {
                _bookService.CreateBook(bookViewModel);

                return Ok();
            }

            catch (BookCreateException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.CreateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetNotFoundMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult DeleteBook([FromRoute] long id)
        {
            try
            {
                _bookService.DeleteBook(id);

                return Ok();
            }

            catch (BookDeleteException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.DeleteNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.DeleteCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }
    }
}
