using AngularLibrary.Helpers;
using AngularLibrary.ViewModels.AuthorizationRequestModels;
using AngularLibrary.ViewModels.AuthorizationViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AngularLibrary.Controllers
{
    [Produces("application/json")]
    [Route("api/Authorization")]
    [AllowAnonymous]
    public class UserController : Controller
    {
        private Services.Interfaces.IApplicationAuthorizationService _authorizationService;
        private ILogger _logger;

        public UserController(Services.Interfaces.IApplicationAuthorizationService authorizationService, ILoggerFactory logger)
        {
            _authorizationService = authorizationService;
            _logger = logger.CreateLogger("Authorization logger");
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody]AuthorizationRequestModel userViewModel)
        {
            try
            {
                var result = await _authorizationService.Register(userViewModel);

                if (!result)
                {
                    return BadRequest("Register is NOT succeeded");
                }

                return Ok("Register succesfull");
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.RegisterErrorMessage);
                return BadRequest(exception);
            }
        }

        [HttpPost]
        [Route("AddAdmin")]
        public async Task<IActionResult> AddAdmin([FromBody]AuthorizationRequestModel userViewModel)
        {
            try
            {
                var result = await _authorizationService.AddAdmin(userViewModel);

                if (!result)
                {
                    return BadRequest("Register of admin is NOT succeeded");
                }

                return Ok("Register as ADMIN succesfull");
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.RegisterErrorMessage);
                return BadRequest(exception);
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] AuthorizationRequestModel userViewModel)
        {
            try
            {
                LoginResponseModel result = null;

                result = await _authorizationService.Login(userViewModel);

                if (result == null)
                {
                    return BadRequest("Login failed");
                }

                return Ok(result);
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.LoginErrorMessage);
                return BadRequest(exception);
            }
        }

        [HttpPost]
        [Route("LogOut")]
        public IActionResult LogOut()
        {
            return Unauthorized();
        }
    }
}
