using AngularLibrary.BusinessLogicLayer.Exceptions.BrochureExceptions;
using AngularLibrary.Helpers;
using AngularLibrary.Services.Interfaces;
using AngularLibrary.ViewModels.BrochureViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AngularLibrary.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize]
    public class BrochureController : Controller
    {
        private IBrochureService _service;
        private ILogger _logger;

        public BrochureController(IBrochureService brochureService, ILoggerFactory logger)
        {
            _service = brochureService;
            _logger = logger.CreateLogger("Brochure logger");
        }

        [HttpGet]
        public async Task<IActionResult> GetBrochures()
        {
            try
            {
                var brochuresList = new GetBrochureViewModel();

                brochuresList = await _service.GetBrochureList();

                return Ok(brochuresList);
            }

            catch (GetBrochureListException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.GetNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.GetCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult UpdateBrochure([FromBody] UpdateBrochureRequestModel brochure)
        {
            try
            {
                _service.UpdateBrochure(brochure);

                return Ok();
            }

            catch (UpdateBrochureException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.UpdateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.UpdateCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpPost]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult CreateBrochure([FromBody] CreateBrochureRequestModel brochure)
        {
            try
            {
                _service.CreateBrochure(brochure);

                return Ok();
            }

            catch (CreateBrochureException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.CreateNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.CreateCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }

        }

        [HttpDelete("{id}")]
        [Authorize(Policy = nameof(Entities.Enums.Policies.AdminRolePolicy))]
        public IActionResult DeleteBrochure([FromRoute] long id)
        {
            try
            {
                _service.DeleteBrochure(id);

                return Ok();
            }

            catch (DeleteBrochureException exception)
            {
                _logger.Log(LogLevel.Error, exception, LogStrings.DeleteNotFoundMessage);

                return NotFound();
            }

            catch (Exception exception)
            {
                _logger.Log(LogLevel.Critical, exception, LogStrings.DeleteCriticalMessage);

                return StatusCode((int)HttpStatusCode.InternalServerError, exception);
            }
        }
    }
}

