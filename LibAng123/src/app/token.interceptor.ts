import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthorizationService } from './services/authorization.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do'; 

@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    private _router: Router;

    constructor( router: Router) {
        this._router = router;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${AuthorizationService.getToken()}`
            }
        });

        return next.handle(request).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {

            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    this._router.navigate(['/authorization/login']);
                }
            }
        });
    }
}
