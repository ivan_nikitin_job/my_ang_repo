import { Component, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../services/authorization.service';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
    selector: 'app-nav-menu',
    templateUrl: './nav-menu.component.html',
    styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

    private _authorizationService: AuthorizationService;
    private _permissionsService: NgxPermissionsService;
    
    constructor( permissionsService: NgxPermissionsService,
        service: AuthorizationService) {

        this._permissionsService = permissionsService;
        this._authorizationService = service;
    }

    ngOnInit() {
        this._permissionsService.loadPermissions(AuthorizationService.getPermission());
    }

    logOut() {
        this._authorizationService.logOut();
        this.ngOnInit();
    }
}
