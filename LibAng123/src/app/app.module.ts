import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { windowProvider } from './window';
import { TokenInterceptor } from './token.interceptor';
import { NavMenuComponent } from './shared/nav-menu/nav-menu.component';

import { NgxPermissionsModule } from 'ngx-permissions';

import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { BooksService } from './services/books.service';
import { MagazineService } from './services/magazine.service';
import { BrochureService } from './services/brochure.service';
import { PublicationHouseService } from './services/publication-house.service';
import { AuthorizationService } from './services/authorization.service';

import { AppRoutingModule } from './app-routing.module';


@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent
    ],

    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        FormsModule,
        GridModule,
        DropDownsModule,
        HttpClientModule,
        HttpModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NgxPermissionsModule.forRoot()
    ],

    providers: [
        HttpClientModule,
        HttpModule,
        windowProvider,
        BooksService,
        MagazineService,
        BrochureService,
        PublicationHouseService,
        AuthorizationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
