
export class GetComplexBookViewModel {

    public bookList?: Array<BookGetComplexBookViewModelItem>;

    constructor() {
        this.bookList = new Array<BookGetComplexBookViewModelItem>();
    }
}

export class BookGetComplexBookViewModelItem {

    public id?: number;
    public title?: string;
    public authorName?: string;
    public yearOfPublish?: number;
    public publicationHouseList?: Array<PublicationHouseViewModelItem>;

    constructor() {
        this.publicationHouseList = new Array<PublicationHouseViewModelItem>();
    }
}

export class PublicationHouseViewModelItem {
    public id?: number;
    public name?: string;
}
