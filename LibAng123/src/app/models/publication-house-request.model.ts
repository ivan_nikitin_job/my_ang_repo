
export class PublicationHouseRequestModel {

    public id?: number;
    public name?: string;
    public address?: string;

    public bookIdList?: Array<number>;

    constructor() {
        this.bookIdList = new Array<number>();
    }
}
