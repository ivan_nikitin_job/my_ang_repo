export class MagazineModel {

    public id?: number;
    public name?: string;
    public issue?: string;
    public yearOfPublishing?: number;

    constructor() { }
}

export class MagazineResponseModel {

    public magazineList?: Array<MagazineModel>

    constructor() {
        this.magazineList = new Array<MagazineModel>();
    }
}
