
export class BookRequestModel {

    public id?: number;
    public title?: string;
    public authorName?: string;
    public yearOfPublish?: number;
    public publicationHouseIdList?: Array<number>;
    
    constructor() {
        this.publicationHouseIdList = new Array<number>();
    }
}
