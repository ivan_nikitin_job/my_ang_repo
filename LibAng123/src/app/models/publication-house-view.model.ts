
export class GetComplexPublicationHouseViewModel {
    public publicationHouseList?: Array<PublicationHouseGetComplexPublicationHouseViewModelItem>

    constructor() {
        this.publicationHouseList = new Array<PublicationHouseGetComplexPublicationHouseViewModelItem>();
    }
}

export class PublicationHouseGetComplexPublicationHouseViewModelItem {

    public id?: number;
    public name?: string;
    public address?: string;
    public bookList?: Array<BookViewModelItem>;

    constructor() {
        this.bookList = new Array<BookViewModelItem>();
    }
}

export class BookViewModelItem {
    public id?: number;
    public title?: string;
}
