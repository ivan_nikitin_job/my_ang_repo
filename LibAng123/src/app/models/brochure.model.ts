import { TypeOfCover } from "../shared/brochure-type-of-cover";

export class BrochureResponseModel {

    public brochureList?: Array<BrochureItem>

    constructor() {
        this.brochureList = new Array<BrochureItem>();
    }
}

export class BrochureItem {

    public id?: number;
    public name?: string;
    public typeOfCover?: TypeOfCover;
    public numberOfPages?: number;

    constructor() { }
}
