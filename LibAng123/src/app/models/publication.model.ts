export class PublicationModel {
  constructor(
    public id?: number,
    public name?: string,
    public type?:string
  ) { }
}

export class PublicationModelList {
    constructor(
        public publicationList?: Array<PublicationModel>
    ) {
        publicationList = new Array<PublicationModel>();
    }
}
