import { TestBed, inject } from '@angular/core/testing';

import { AllPublicationsService } from './all-publications.service';

describe('AllPublicationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllPublicationsService]
    });
  });

  it('should be created', inject([AllPublicationsService], (service: AllPublicationsService) => {
    expect(service).toBeTruthy();
  }));
});
