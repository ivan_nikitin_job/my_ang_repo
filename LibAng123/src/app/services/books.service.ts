import { Injectable } from '@angular/core';
import { HttpClient, JsonpClientBackend, HttpClientJsonpModule } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { tap } from 'rxjs/operators/tap';
import { map } from 'rxjs/operators/map';

import { BookRequestModel } from '../models/book-request.model';
import { GetComplexBookViewModel, BookGetComplexBookViewModelItem, PublicationHouseViewModelItem } from '../models/book-view.model';
import { PublicationHouseRequestModel } from "../models/publication-house-request.model";
import { GetComplexPublicationHouseViewModel } from '../models/publication-house-view.model';

import { AuthorizationService } from '../services/authorization.service';

const CREATE_ACTION = 'Create';
const UPDATE_ACTION = 'Update';
const REMOVE_ACTION = 'Destroy';

@Injectable()
export class BooksService extends BehaviorSubject<Array<any>> {
    private _http: HttpClient

    constructor(http: HttpClient) {
        super([]);
        this._http = http;
    }
    public data: Array<any> = Array<any>();

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
            tap(data => {
                this.data = data;
            }))
            .subscribe(data => {
                super.next(data);
            });
    }

    public save(data: any, selectedPublicationHouseList: any, isNew?: boolean) {
        const action = isNew ? CREATE_ACTION : UPDATE_ACTION;

        this.reset();
        data.selectedPublicationHouseList = selectedPublicationHouseList;
        
        this.fetch(action, data).subscribe(() => this.read(), () => this.read());
    }

    public remove(data: any) {
        this.reset();

        this.fetch(REMOVE_ACTION, data).subscribe(() => this.read(), () => this.read());
    }

    public resetItem(dataItem: any) {

        if (!dataItem) {
            return;
        }

        let originalDataItem = this.data.find(item => item.id === dataItem.id);

        Object.assign(originalDataItem, dataItem);

        super.next(this.data);
    }

    private reset() {
        this.data = new Array<any>();
    }

    private fetch(action: string = '', data?: any): Observable<Array<any>> {
        if (action == '') {

            let result = this._http.get<GetComplexBookViewModel>('/api/Book')
                .map(response => {
                    return response.bookList;
                });

            return result;
        }

        if (action == 'Create') {
            let book = new BookRequestModel();

            book.authorName = data.authorName;
            book.title = data.title;
            book.yearOfPublish = data.yearOfPublish;

            if (data.selectedPublicationHouseList) {

                for (let i = 0; i < data.selectedPublicationHouseList.length; i++) {

                    book.publicationHouseIdList[i] = data.selectedPublicationHouseList[i].id;
                }
            }

            return this._http.post('/api/Book', book).pipe(map(response => <Array<any>>response));
        }

        if (action == 'Update') {

            let book = new BookRequestModel();

            book.id = data.id;
            book.authorName = data.authorName;
            book.title = data.title;
            book.yearOfPublish = data.yearOfPublish;

            if (data.selectedPublicationHouseList) {

                for (let i = 0; i < data.selectedPublicationHouseList.length; i++) {

                    book.publicationHouseIdList[i] = data.selectedPublicationHouseList[i].id;
                }
            }

            return this._http.put('/api/Book/' + book.id, book).pipe(map(response => <Array<any>>response));
        }

        if (action == 'Destroy') {

            let book = data;

            return this._http.delete('/api/Book/' + book.id).pipe(map(response => <Array<any>>response));
        }

        return null;
    }

    loadAllPublicationHouses() {

        let result = this._http.get<any>('/api/GetSimplePublicationHouses')
            .map(response => {
                return response.publicationHouseList
            });

        return result;
    }

    loadPublicationHouseId(id: number) {

        let result = this._http.get<any>('/api/GetPublicationHouseForBook/' + id)
            .map(response => {
                return response.publicationHouseList
            });

        return result;
    }
}
