import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import {
    HttpClient,
    JsonpClientBackend,
    HttpClientJsonpModule,
    HttpHeaders,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';

import { map } from 'rxjs/operators/map';

import { tokenNotExpired } from 'angular2-jwt';

import { UserViewModel } from '../models/user-view.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthorizationService {

    private _authorizationStatusSource = new BehaviorSubject<boolean>(false);
    private _http: HttpClient
    
    constructor(http: HttpClient) {
        this._http = http;
    }

    public regiser(data: UserViewModel): Observable<any> {
        return this._http.post('api/Authorization/Register', data)
            .pipe(
            map((x) => {
                return x;
            }));
    }

    public login(data: UserViewModel) {

        let authorizationToken: string;
        let permission: string;

        return this._http.post('api/Authorization/Login',
            data)
            .map((response: any) => {
                authorizationToken = response.authorizationToken;
                permission = response.permission;
                localStorage.setItem('authorizationToken', authorizationToken);
                localStorage.setItem('permission', permission);
                this._authorizationStatusSource.next(true);
            });
    }

    public logOut() {

        let response = this._http.post('api/Authorization/LogOut', '').subscribe(message => console.log(message));
        localStorage.clear();

        return response;
    }

    public regiserAsAdmin(data: UserViewModel): Observable<any> {

        return this._http.post('api/Authorization/AddAdmin', data)
            .pipe(map((x) => {
                return x;
            }));
    }

    public static getPermission(): Array<string> {

        let permissions = new Array<string>();
        let isAuthentificated: boolean = this.isAuthentificated();

        if (isAuthentificated) {
            let loadedPermission = localStorage.getItem('permission');
            permissions.push(loadedPermission);
        }

        return permissions;
    }

    public static getToken(): string {
        return localStorage.getItem('authorizationToken');
    }

    public static isAuthentificated(): boolean {
        return tokenNotExpired(null, this.getToken());
    }
}

