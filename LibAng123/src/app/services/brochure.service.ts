import { Injectable } from '@angular/core';
import { HttpClient, JsonpClientBackend, HttpClientJsonpModule } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { BrochureItem, BrochureResponseModel } from "../models/brochure.model";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { AuthorizationService } from './authorization.service';

import { tap } from 'rxjs/operators/tap';
import { map } from 'rxjs/operators/map';

const CREATE_ACTION = 'Create';
const UPDATE_ACTION = 'Update';
const REMOVE_ACTION = 'Destroy';

@Injectable()
export class BrochureService extends BehaviorSubject<Array<any>> {

    private _http: HttpClient

    constructor(http: HttpClient) {
        super([]);
        this._http = http;
    }

    public data: Array<any> = Array<any>();

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
            tap(data => {
                this.data = data;
            }))
            .subscribe(data => {
                super.next(data);
            });
    }

    public save(data: any, isNew?: boolean) {

        const action = isNew ? CREATE_ACTION : UPDATE_ACTION;

        this.reset();

        this.fetch(action, data).subscribe(() => this.read(), () => this.read());
    }

    public remove(data: any) {

        this.reset();

        this.fetch(REMOVE_ACTION, data)
            .subscribe(() => this.read(), () => this.read());
    }

    public resetItem(dataItem: any) {

        if (!dataItem) {
            return;
        }

        const originalDataItem = this.data.find(item => item.id === dataItem.id);

        Object.assign(originalDataItem, dataItem);

        super.next(this.data);
    }

    private reset() {
        this.data = new Array<any>();
    }

    private fetch(action: string = '', data?: any): Observable<Array<any>> {
        if (action == '') {

            let result = this._http.get<BrochureResponseModel>('/api/Brochure')
                .map(response => {
                    return response.brochureList;
                });

            return result;
        }
        if (action == 'Create') {

            let brochure: BrochureItem = data;
            brochure.id = 0;

            return this._http.post('/api/Brochure', brochure).pipe(map(response => <Array<any>>response));

        }
        if (action == 'Update') {

            let brochure: BrochureItem = data;

            return this._http.put('/api/Brochure/' + brochure.id, brochure).pipe(map(response => <Array<any>>response));

        }
        if (action == 'Destroy') {

            let brochure: BrochureItem = data;

            return this._http.delete('/api/Brochure/' + brochure.id).pipe(map(response => <Array<any>>response));
        }
    }
}
