import { Injectable } from '@angular/core';
import { HttpClient, JsonpClientBackend, HttpClientJsonpModule } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { tap } from 'rxjs/operators/tap';
import { map } from 'rxjs/operators/map';

import { BookRequestModel } from '../models/book-request.model';
import { BookGetComplexBookViewModelItem, GetComplexBookViewModel, PublicationHouseViewModelItem } from '../models/book-view.model';
import { PublicationHouseRequestModel } from "../models/publication-house-request.model";
import { GetComplexPublicationHouseViewModel } from '../models/publication-house-view.model';

import { AuthorizationService } from './authorization.service';

const CREATE_ACTION = 'Create';
const UPDATE_ACTION = 'Update';
const REMOVE_ACTION = 'Destroy';

@Injectable()
export class PublicationHouseService extends BehaviorSubject<Array<any>> {
    private _http: HttpClient

    constructor(http: HttpClient) {
        super([]);
        this._http = http;
    }

    public data: Array<any> = new Array<any>();

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
            tap(data => {
                this.data = data;
            })
            )
            .subscribe(data => {
                super.next(data);
            });
    }

    public save(data: any, selectedBookList: any, isNew?: boolean) {

        const action = isNew ? CREATE_ACTION : UPDATE_ACTION;

        this.reset();
        data.selectedBookList = selectedBookList;

        this.fetch(action, data)
            .subscribe(() => this.read(), () => this.read());
    }

    public remove(data: any) {

        this.reset();

        this.fetch(REMOVE_ACTION, data)
            .subscribe(() => this.read(), () => this.read());
    }

    public resetItem(dataItem: any) {

        if (!dataItem) {
            return;
        }

        let originalDataItem = this.data.find(item => item.id === dataItem.id);

        Object.assign(originalDataItem, dataItem);

        super.next(this.data);
    }

    private reset() {
        this.data = new Array<any>();
    }

    private fetch(action: string = '', data?: any): Observable<Array<any>> {
        if (action == '') {

            var result = this._http.get<GetComplexPublicationHouseViewModel>('/api/PublicationHouse')
                .map(response => {
                    return response.publicationHouseList
                });

            return result;
        }

        if (action == 'Create') {

            let publicationHouse = new PublicationHouseRequestModel();

            publicationHouse.name = data.name;
            publicationHouse.address = data.address;

            for (let i = 0; i < data.selectedBookList.length; i++) {

                publicationHouse.bookIdList[i] = data.selectedBookList[i].id;
            }

            return this._http.post('/api/PublicationHouse', publicationHouse).pipe(map(response => <Array<any>>response));
        }

        if (action == 'Update') {

            let publicationHouse = new PublicationHouseRequestModel();

            publicationHouse.id = data.id;
            publicationHouse.name = data.name;
            publicationHouse.address = data.address;

            for (let i = 0; i < data.selectedBookList.length; i++) {

                publicationHouse.bookIdList[i] = data.selectedBookList[i].id;

            }

            return this._http.put('/api/PublicationHouse/' + publicationHouse.id, publicationHouse).pipe(map(response => <Array<any>>response));
        }

        if (action == 'Destroy') {

            let publicationHouse: PublicationHouseRequestModel = data;
            return this._http.delete('/api/PublicationHouse/' + publicationHouse.id).pipe(map(response => <Array<any>>response));
        }
    }

    loadAllBooks() {

        let result = this._http.get<any>('/api/GetSimpleBooks')
            .map(response => {
                return response.bookList;
            });

        return result;
    }

    loadBooksId(id: number) {

        let result = this._http.get<any>('/api/GetBookForPublicationHouse/' + id)
            .map(response => {
                return response.bookList;
            });

        return result;
    }
}
