import { Injectable } from '@angular/core';
import { PublicationModel, PublicationModelList } from '../models/publication.model';
import { HttpClient, JsonpClientBackend, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { tap } from 'rxjs/operators/tap';
import { map } from 'rxjs/operators/map';

import { AuthorizationService } from '../services/authorization.service';

@Injectable()
export class AllPublicationsService extends BehaviorSubject<any[]> {
    constructor(private http: HttpClient) {
        super([]);
    }

    private data: any[] = [];

    private fetch(): Observable<any[]> {
        var result = this.http.get<PublicationModelList>('/api/Publication')
            .map(res => {
                return res.publicationList
            });
        return result;
    }

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
            tap(data => {
                this.data = data;
            })
            )
            .subscribe(data => {
                super.next(data);
            });
    }
}
