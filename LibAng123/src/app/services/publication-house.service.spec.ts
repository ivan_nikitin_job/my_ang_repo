import { TestBed, inject } from '@angular/core/testing';

import { PublicationHouseService } from './publication-house.service';

describe('PublicationHouseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PublicationHouseService]
    });
  });

  it('should be created', inject([PublicationHouseService], (service: PublicationHouseService) => {
    expect(service).toBeTruthy();
  }));
});
