import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { NgxPermissionsModule } from 'ngx-permissions';

import { BookListComponent } from './components/book/book-list/book-list.component';
import { MagazineListComponent } from './components/magazine/magazine-list/magazine-list.component';
import { BrochureListComponent } from './components/brochure/brochure-list/brochure-list.component';
import { PublicationHouseListComponent } from './components/publication-house/publication-house-list/publication-house-list.component';


const appRoutes: Routes = [
    { path: '', component: BookListComponent, pathMatch: 'full' },
    { path: 'magazine', component: MagazineListComponent, pathMatch: 'full' },
    { path: 'brochure', component: BrochureListComponent, pathMatch: 'full' },
    { path: 'publicationHouse', component: PublicationHouseListComponent, pathMatch: 'full' },
    { path: 'authorization', loadChildren: './components/authorization/authorization.module#AuthorizationModule' }
];


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        GridModule,
        DropDownsModule,
        RouterModule.forRoot(appRoutes),
        NgxPermissionsModule.forRoot()
    ],

    exports: [
        RouterModule
    ],

    declarations: [
        BookListComponent,
        MagazineListComponent,
        BrochureListComponent,
        PublicationHouseListComponent
    ]
})
export class AppRoutingModule { }
