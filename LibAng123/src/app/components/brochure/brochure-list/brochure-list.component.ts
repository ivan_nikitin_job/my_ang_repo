import { Component, OnInit, Inject, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { BrochureService } from "../../../services/brochure.service";
import { BrochureItem } from "../../../models/brochure.model";
import { TypeOfCover } from "../../../shared/brochure-type-of-cover"
import { windowProvider } from '../../../window';

import { Observable } from "rxjs/Observable";
import { map } from 'rxjs/operators/map';

import { NgxPermissionsService } from 'ngx-permissions';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { AuthorizationService } from '../../../services/authorization.service';


@Component({
    selector: 'brochure-list',
    templateUrl: './brochure-list.component.html',
    styleUrls: ['./brochure-list.component.css']
})
export class BrochureListComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;

    public TypeOfCover = TypeOfCover;

    private _brochureService: BrochureService;
    private _editedRowIndex: number;
    private _editedProduct: BrochureItem;
    private _permissionsService: NgxPermissionsService

    constructor( @Inject(BrochureService) brochureServiceFactory: any,
        permissionsService: NgxPermissionsService) {

        this._brochureService = brochureServiceFactory;
        this._permissionsService = permissionsService;
    }

    public ngOnInit(): void {
        let permission: string[] = AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);

        this.view = this._brochureService.pipe(map(data => process(data, this.gridState)));
        this._brochureService.read();
    }

    public onStateChange(state: State) {
        this.gridState = state;

        this._brochureService.read();
    }

    public addHandler({ sender }, formInstance) {
        formInstance.reset();
        this.closeEditor(sender);

        sender.addRow(new BrochureItem());
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);
        this._editedRowIndex = rowIndex;
        this._editedProduct = Object.assign({}, dataItem);

        sender.editRow(rowIndex);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, dataItem, isNew }) {
        this._brochureService.save(dataItem, isNew);

        sender.closeRow(rowIndex);

        this._editedRowIndex = null;
        this._editedProduct = null;
    }

    public removeHandler({ dataItem }) {
        this._brochureService.remove(dataItem);
    }

    private closeEditor(grid, rowIndex = this._editedRowIndex) {
        grid.closeRow(rowIndex);
        this._brochureService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    }
}
