import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPublicationsListComponent } from './all-publications-list.component';

describe('AllPublicationsListComponent', () => {
  let component: AllPublicationsListComponent;
  let fixture: ComponentFixture<AllPublicationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPublicationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPublicationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
