import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';

import { AllPublicationsService } from '../../../services/all-publications.service';
import { AuthorizationService } from '../../../services/authorization.service'
import { PublicationModel } from '../../../models/publication.model';

import { NgxPermissionsService } from 'ngx-permissions';

import { State, process } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';

@Component({
    selector: 'app-all-publications-list',
    templateUrl: './all-publications-list.component.html',
    styleUrls: ['./all-publications-list.component.css']
})
export class AllPublicationsListComponent {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    private pubService: AllPublicationsService;

    constructor(
        @Inject(AllPublicationsService) allPublicationsServiceFactory: any,
        private permissionsService: NgxPermissionsService
    ) {
        this.pubService = allPublicationsServiceFactory;
    }
    public ngOnInit(): void {
        this.view = this.pubService.pipe(map(data => process(data, this.gridState)));
        this.pubService.read();
        this.permissionsService.loadPermissions(AuthorizationService.getPermission());
    }
    public onStateChange(state: State) {
        this.gridState = state;

        this.pubService.read();
    }
}
