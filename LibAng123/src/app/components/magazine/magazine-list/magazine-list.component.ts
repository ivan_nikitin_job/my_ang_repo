import { Component, OnInit, Inject, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { MagazineService } from "../../../services/magazine.service";
import { MagazineModel } from "../../../models/magazine.model";
import { AuthorizationService } from '../../../services/authorization.service';

import { Observable } from "rxjs/Observable";
import { map } from 'rxjs/operators/map';
import { windowProvider } from '../../../window';

import { NgxPermissionsService } from 'ngx-permissions';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';

@Component({
    selector: 'magazine-list',
    templateUrl: './magazine-list.component.html',
    styleUrls: ['./magazine-list.component.css']
})
export class MagazineListComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;

    private _magazineService: MagazineService;
    private _editedRowIndex: number;
    private _editedProduct: MagazineModel;
    private _permissionsService: NgxPermissionsService

    constructor( @Inject(MagazineService) magazineServiceFactory: any,
        permissionsService: NgxPermissionsService) {

        this._magazineService = magazineServiceFactory;
        this._permissionsService = permissionsService;
    }

    public ngOnInit(): void {
        let permission: string[] = AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);
        this.view = this._magazineService.pipe(map(data => process(data, this.gridState)));

        this._magazineService.read();
    }

    public onStateChange(state: State) {
        this.gridState = state;

        this._magazineService.read();
    }

    public addHandler({ sender }, formInstance) {
        formInstance.reset();
        this.closeEditor(sender);

        sender.addRow(new MagazineModel());
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);

        this._editedRowIndex = rowIndex;
        this._editedProduct = Object.assign({}, dataItem);

        sender.editRow(rowIndex);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, dataItem, isNew }) {
        this._magazineService.save(dataItem, isNew);

        sender.closeRow(rowIndex);

        this._editedRowIndex = null;
        this._editedProduct = null;
    }

    public removeHandler({ dataItem }) {
        this._magazineService.remove(dataItem);
    }

    private closeEditor(grid, rowIndex = this._editedRowIndex) {
        grid.closeRow(rowIndex);
        this._magazineService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    }
}
