import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { NgxPermissionsService } from 'ngx-permissions';

import { AuthorizationService } from '../../../services/authorization.service';
import { UserViewModel } from '../../../models/user-view.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public user: UserViewModel;

    private _authorizationService: AuthorizationService;
    private _router: Router
    
    constructor(private authorizationServiceFactory: AuthorizationService, router: Router) {

        this._authorizationService = authorizationServiceFactory;
        this._router = router;
    }

    ngOnInit() {
        this.user = new UserViewModel();
    }

    public login() {
        this._authorizationService.login(this.user).subscribe(data => {
            this._router.navigate(['/']);
        });
    }

    public toRegister() {
        this._router.navigate(['/authorization/register']);
    }
}
