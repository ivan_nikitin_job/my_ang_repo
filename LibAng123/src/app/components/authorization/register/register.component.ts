import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { NgxPermissionsService } from 'ngx-permissions';

import { AuthorizationService } from '../../../services/authorization.service';
import { UserViewModel } from '../../../models/user-view.model';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    public user: UserViewModel;
    public adminCheckbox: boolean = false;
    private _authorizationService: AuthorizationService;
    private _router: Router;

    constructor( @Inject(AuthorizationService) AuthorizationServiceFactory: any,
        router: Router) {
        this._authorizationService = AuthorizationServiceFactory;
        this._router = router;
    }

    ngOnInit() {
        this.user = new UserViewModel();
    }

    register() {
        if (this.adminCheckbox) {
            this._authorizationService.regiserAsAdmin(this.user).subscribe(error => console.log(error));
            this._router.navigateByUrl("/");
            return;
        }

        this._authorizationService.regiser(this.user).subscribe(error => console.log(error));
        this._router.navigateByUrl("/");
    }
}
