import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import { AuthorizationRoutingModule } from './authorization-routing.module';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        AuthorizationRoutingModule
    ],
    declarations: [
        LoginComponent,
        RegisterComponent
    ]
})
export class AuthorizationModule { }
