import { Component, OnInit, Inject, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { BooksService } from "../../../services/books.service";

import { BookRequestModel } from "../../../models/book-request.model";
import { BookGetComplexBookViewModelItem, PublicationHouseViewModelItem, GetComplexBookViewModel } from '../../../models/book-view.model';
import { PublicationHouseRequestModel } from '../../../models/publication-house-request.model';

import { AuthorizationService } from '../../../services/authorization.service';

import { Observable } from "rxjs/Observable";
import { map } from 'rxjs/operators/map';
import { windowProvider } from '../../../window';

import { NgxPermissionsService } from 'ngx-permissions';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { MultiSelectComponent } from '@progress/kendo-angular-dropdowns';

@Component({
    selector: 'book-list',
    templateUrl: './book-list.component.html',
    styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;

    public allPublicationHouseList: Array<PublicationHouseRequestModel> = new Array<PublicationHouseRequestModel>();
    public selectedPublicationHouseList: Array<PublicationHouseRequestModel> = new Array<PublicationHouseRequestModel>();
    
    private _bookService: BooksService;
    private _editedRowIndex: number;
    private _editedProduct: BookRequestModel;
    private _permissionsService: NgxPermissionsService

    constructor( @Inject(BooksService) bookServiceFactory: any,
        router: Router,
        permissionsService: NgxPermissionsService) {

        this._bookService = bookServiceFactory;
        this._permissionsService = permissionsService;
    }

    public ngOnInit(): void {
        let permission: string[] = AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);
        this.view = this._bookService.pipe(map(data => process(data, this.gridState)));
        this._bookService.read();
    }

    public onStateChange(state: State) {
        this.gridState = state;
        this._bookService.read();
    }

    public addHandler({ sender }, formInstance) {
        formInstance.reset();
        this.closeEditor(sender);
        this._bookService.loadAllPublicationHouses().subscribe(data => this.allPublicationHouseList = data);
        this.selectedPublicationHouseList = [];
        sender.addRow(new PublicationHouseRequestModel());
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);
        this._editedProduct = Object.assign({}, dataItem);
        this._bookService.loadAllPublicationHouses().subscribe(data => this.allPublicationHouseList = data);
        this._bookService.loadPublicationHouseId(this._editedProduct.id).subscribe(data => {
            this.selectedPublicationHouseList = data;
        });
        sender.editRow(rowIndex);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, dataItem, isNew }) {
        this._bookService.save(dataItem, this.selectedPublicationHouseList, isNew);

        sender.closeRow(rowIndex);

        this._editedRowIndex = null;
        this._editedProduct = null;
    }

    public removeHandler({ dataItem }) {
        this._bookService.remove(dataItem);
    }

    private closeEditor(grid, rowIndex = this._editedRowIndex) {
        grid.closeRow(rowIndex);
        this._bookService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    }
}
