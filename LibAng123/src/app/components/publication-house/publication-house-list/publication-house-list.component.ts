import { Component, OnInit, Inject, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { PublicationHouseService } from "../../../services/publication-house.service";

import { BookRequestModel } from "../../../models/book-request.model";
import { PublicationHouseRequestModel } from '../../../models/publication-house-request.model';
import { GetComplexPublicationHouseViewModel, PublicationHouseGetComplexPublicationHouseViewModelItem, BookViewModelItem } from '../../../models/publication-house-view.model';

import { windowProvider } from '../../../window';

import { Observable } from "rxjs/Observable";
import { map } from 'rxjs/operators/map';

import { NgxPermissionsService } from 'ngx-permissions';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { MultiSelectComponent } from '@progress/kendo-angular-dropdowns';
import { AuthorizationService } from '../../../services/authorization.service';

@Component({
    selector: 'publication-house-list',
    templateUrl: './publication-house-list.component.html',
    styleUrls: ['./publication-house-list.component.css']
})
export class PublicationHouseListComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;

    public allBookList: Array<BookRequestModel> = new Array<BookRequestModel>();
    public selectedBookList: Array<BookRequestModel> = new Array<BookRequestModel>();


    private _publicationHouseService: PublicationHouseService;
    private _editedRowIndex: number;
    private _editedProduct: PublicationHouseRequestModel;
    private _permissionsService: NgxPermissionsService

    constructor( @Inject(PublicationHouseService) publicationHouseServiceFactory: any,
        permissionsService: NgxPermissionsService) {

        this._publicationHouseService = publicationHouseServiceFactory;
        this._permissionsService = permissionsService;
    }

    public ngOnInit(): void {
        let permission: string[] = AuthorizationService.getPermission();
        this._permissionsService.loadPermissions(permission);
        this.view = this._publicationHouseService.pipe(map(data => process(data, this.gridState)));
        this._publicationHouseService.read();
    }

    public onStateChange(state: State) {
        this.gridState = state;
        this._publicationHouseService.read();
    }

    public addHandler({ sender }, formInstance) {
        formInstance.reset();
        this.closeEditor(sender);
        this._publicationHouseService.loadAllBooks().subscribe(data => this.allBookList = data);
        this.selectedBookList = [];
        sender.addRow(new PublicationHouseRequestModel());
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);

        this._editedProduct = Object.assign({}, dataItem);

        this._publicationHouseService.loadAllBooks().subscribe(data => this.allBookList = data);
        this._publicationHouseService.loadBooksId(this._editedProduct.id).subscribe(data => {
            this.selectedBookList = data;
        });

        sender.editRow(rowIndex);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, dataItem, isNew }) {
        this._publicationHouseService.save(dataItem, this.selectedBookList, isNew);

        sender.closeRow(rowIndex);

        this._editedRowIndex = null;
        this._editedProduct = null;
    }

    public removeHandler({ dataItem }) {
        this._publicationHouseService.remove(dataItem);
    }

    private closeEditor(grid, rowIndex = this._editedRowIndex) {
        grid.closeRow(rowIndex);
        this._publicationHouseService.resetItem(this._editedProduct);
        this._editedRowIndex = null;
        this._editedProduct = null;
    }
}
