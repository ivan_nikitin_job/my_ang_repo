import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationHouseListComponent } from './publication-house-list.component';

describe('PublicationHouseListComponent', () => {
  let component: PublicationHouseListComponent;
  let fixture: ComponentFixture<PublicationHouseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationHouseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationHouseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
