using Microsoft.IdentityModel.Tokens;
using System;

namespace AngularLibrary.JWT.Configurations
{
    public class JwtConfigurations
    {
        public string Issuer { get; set; }
        public bool ValidateIssuer { get; set; }
        public bool ValidateIssuerSigningKey { get; set; }

        public string Subject { get; set; }

        public string Audience { get; set; }
        public bool ValidateAudience { get; set; }

        public DateTime Expiration => IssuedAt.Add(ValidFor);
        public bool RequireExpirationTime { get; set; }
        public bool ValidateLifetime { get; set; }

        public DateTime NotBefore => DateTime.UtcNow;

        public DateTime IssuedAt => DateTime.UtcNow;

        public TimeSpan ValidFor { get; set; } = TimeSpan.FromMinutes(120);

        public string JtiGenerator = "324D3136-AE62-426D-B72A-F1CB9016E152";

        public SigningCredentials SigningCredentials { get; set; }
    }
}
