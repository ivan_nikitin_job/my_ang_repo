﻿namespace AngularLibrary.JWT.Helpers
{
    public class JWTStrings
    {
        public const string ApiAccess = "api_access",
                            ClaimIdentifierRole = "role",
                            ClaimIdentifierID = "id",

                            IdentifierRoleUser = "user",
                            IdentifierRoleAdmin = "admin",

                            AppUserRoleName = "AppUser",
                            AppAdminRoleName = "AppAdmin";
    }
}
