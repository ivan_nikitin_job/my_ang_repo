using AngularLibrary.JWT.Configurations;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;

namespace AngularLibrary.JWT.TokenGeneration
{
    public class JwtFactory : IJwtFactory
    {
        private readonly JwtConfigurations _jwtOptions;

        public JwtFactory(JwtConfigurations configurations)
        {
            _jwtOptions = configurations;
            ThrowIfInvalidOptions(_jwtOptions);
        }

        public ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string role)
        {
            var identity = new ClaimsIdentity(new GenericIdentity(userName, "Token"), new[]
                {
                    new Claim(Helpers.JWTStrings.ClaimIdentifierID, id),
                    new Claim(Helpers.JWTStrings.ClaimIdentifierRole, role)
                });

            return identity;
        }

        public string GenerateEncodedToken(string userName, ClaimsIdentity identity)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, _jwtOptions.JtiGenerator),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(),ClaimValueTypes.Integer64),
                identity.FindFirst(Helpers.JWTStrings.ClaimIdentifierRole),
                identity.FindFirst(Helpers.JWTStrings.ClaimIdentifierID)
            };

            var jwt = new JwtSecurityToken
            (
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: _jwtOptions.SigningCredentials
            );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        private static long ToUnixEpochDate(DateTime date)
        {
            var result = (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
            return result;
        }

        private static void ThrowIfInvalidOptions(JwtConfigurations configuration)
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            if (configuration.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtConfigurations.ValidFor));
            }

            if (configuration.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtConfigurations.SigningCredentials));
            }

            if (configuration.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtConfigurations.JtiGenerator));
            }
        }
    }
}
