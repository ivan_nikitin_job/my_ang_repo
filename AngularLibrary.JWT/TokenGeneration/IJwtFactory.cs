using System.Security.Claims;

namespace AngularLibrary.JWT.TokenGeneration
{
    public interface IJwtFactory
    {
        string GenerateEncodedToken(string userName, ClaimsIdentity identity);
        ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string role);
    }
}
