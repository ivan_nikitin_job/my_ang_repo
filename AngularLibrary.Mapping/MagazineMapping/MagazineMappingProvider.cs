﻿using AngularLibrary.Entities;
using AngularLibrary.ViewModels.MagazineViewModels;
using System.Collections.Generic;

namespace AngularLibrary.Mapping.MagazineMapping
{
    public static class MagazineMappingProvider
    {
        public static GetMagazineViewModel MapMagazineToMagazineGetMagazineViewModel(IEnumerable<Magazine> magazineList)
        {
            var result = new GetMagazineViewModel();

            foreach (var magazine in magazineList)
            {
                var magazineViewItem = new MagazineGetMagazineViewModelItem
                {
                    Id = magazine.Id,
                    Name = magazine.Name,
                    Issue = magazine.Issue,
                    YearOfPublishing = magazine.YearOfPublishing
                };

                result.MagazineList.Add(magazineViewItem);
            }

            return result;
        }

        public static Magazine MapCreateMagazineRequestModelToMagazine(CreateMagazineRequestModel magazineRequestModel)
        {
            var result = new Magazine
            {
                Name = magazineRequestModel.Name,
                Issue = magazineRequestModel.Issue,
                YearOfPublishing = magazineRequestModel.YearOfPublishing
            };

            return result;
        }

        public static Magazine MapUpdateMagazineRequestModelToMagazine(UpdateMagazineRequestModel magazineRequestModel)
        {
            var result = new Magazine
            {
                Id = magazineRequestModel.Id,
                Name = magazineRequestModel.Name,
                Issue = magazineRequestModel.Issue,
                YearOfPublishing = magazineRequestModel.YearOfPublishing
            };

            return result;
        }
    }
}
