﻿using AngularLibrary.Entities;
using AngularLibrary.ViewModels.BrochureViewModels;
using AngularLibrary.ViewModels.Enums;
using System.Collections.Generic;

namespace AngularLibrary.Mapping.BrochureMapping
{
    public static class BrochureMappingProvider
    {
        public static GetBrochureViewModel MapBrochureListToGetBrochureViewModel(IEnumerable<Brochure> brochureList)
        {
            var result = new GetBrochureViewModel();

            foreach (var brochure in brochureList)
            {
                int typeOfCover = (int)brochure.TypeOfCover;

                var brochureViewItem = new BrochureGetBrochureViewModelItem()
                {
                    Id = brochure.Id,
                    Name = brochure.Name,
                    NumberOfPages = brochure.NumberOfPages,
                    TypeOfCover = (TypeOfCoverViewModel)typeOfCover
                };

                result.BrochureList.Add(brochureViewItem);
            }

            return result;
        }

        public static Brochure MapCreateBrochureRequestModelToBrochure(CreateBrochureRequestModel brochureRequestModel)
        {
            int typeOfCover = (int)brochureRequestModel.TypeOfCover;

            var result = new Brochure()
            {
                Name = brochureRequestModel.Name,
                NumberOfPages = brochureRequestModel.NumberOfPages,
                TypeOfCover = (TypeOfCover)typeOfCover
            };

            return result;
        }

        public static Brochure MapUpdateBrochureRequestModelToBrochure(UpdateBrochureRequestModel brochureRequestModel)
        {
            int typeOfCover = (int)brochureRequestModel.TypeOfCover;

            var result = new Brochure
            {
                Id = brochureRequestModel.Id,
                Name = brochureRequestModel.Name,
                NumberOfPages = brochureRequestModel.NumberOfPages,
                TypeOfCover = (TypeOfCover)typeOfCover
            };

            return result;
        }
    }
}
