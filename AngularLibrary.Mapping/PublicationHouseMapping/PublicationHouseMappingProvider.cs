﻿using AngularLibrary.Entities;
using AngularLibrary.ViewModels.PublicationHouseViewModels;
using System.Collections.Generic;
using System.Linq;

namespace AngularLibrary.Mapping.PublicationHouseMapping
{
    public class PublicationHouseMappingProvider
    {
        public static PublicationHouse MapCreatePublicationHouseViewModelToPublicationHouse(CreatePublicationHouseRequestModel publicationHouseRequestModel)
        {
            var result = new PublicationHouse
            {
                Name = publicationHouseRequestModel.Name,
                Address = publicationHouseRequestModel.Address
            };

            return result;
        }

        public static PublicationHouse MapUpdatePublicationHouseRequestModelToPublicationHouse(UpdatePublicationHouseRequestModel publicationHouseRequestModel)
        {
            var result = new PublicationHouse
            {
                Id = publicationHouseRequestModel.Id,
                Name = publicationHouseRequestModel.Name,
                Address = publicationHouseRequestModel.Address
            };

            return result;
        }

        public static IEnumerable<BookViewModelItem> MapBookListToModel(IEnumerable<Book> bookList)
        {
            var result = new List<BookViewModelItem>();
            foreach (var book in bookList)
            {
                result.Add(new BookViewModelItem
                {
                    Id = book.Id,
                    Title = book.Title
                });
            }

            return result;
        }

        public static GetSimplePublicationHouseViewModel MapPublicationHouseListToModel(IEnumerable<PublicationHouse> publicationHouseList)
        {
            var result = new GetSimplePublicationHouseViewModel();

            foreach (var publicationHouse in publicationHouseList)
            {
                result.PublicationHouseList.Add(new PublicationHouseGetSimplePublicationHouseViewModelItem
                {
                    Id = publicationHouse.Id,
                    Name = publicationHouse.Name,
                    Address = publicationHouse.Address
                });
            }

            return result;
        }

        public static GetComplexPublicationHouseViewModel MapGetComplexPublicationHouseViewModel(IEnumerable<Book> bookList, IEnumerable<PublicationHouse> publicationHouseList, IEnumerable<BookPublicationHouse> relationList)
        {
            var result = new GetComplexPublicationHouseViewModel();

            foreach (var publicationHouse in publicationHouseList)
            {
                var publicationHouseViewItem = new PublicationHouseGetComplexPublicationHouseViewModelItem
                {
                    Id = publicationHouse.Id,
                    Name = publicationHouse.Name,
                    Address = publicationHouse.Address
                };

                var tmpRelation = relationList.Where(obj => obj.PublicationHouseId == publicationHouse.Id);

                var publicationHouseIds = tmpRelation.Select(obj => obj.BookId);

                var tempBooks = bookList.Where(obj => publicationHouseIds.Contains(obj.Id));

                publicationHouseViewItem.BookList = MapBookListToModel(tempBooks);

                result.PublicationHouseList.Add(publicationHouseViewItem);
            }

            return result;
        }
    }
}
