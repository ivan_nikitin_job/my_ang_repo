﻿using AngularLibrary.Entities;
using AngularLibrary.ViewModels.BookViewModels;
using System.Collections.Generic;
using System.Linq;

namespace AngularLibrary.Mapping.BookMapping
{
    public static class BookMapperProvider
    {
        public static Book MapCreateBookRequestModelToBook(CreateBookRequestModel bookModel)
        {
            var result = new Book()
            {
                Title = bookModel.Title,
                AuthorName = bookModel.AuthorName,
                YearOfPublish = bookModel.YearOfPublish
            };

            return result;
        }

        public static Book MapUpdateBookRequestModelToBook(UpdateBookRequestModel bookModel)
        {
            var result = new Book()
            {
                Id = bookModel.Id,
                Title = bookModel.Title,
                AuthorName = bookModel.AuthorName,
                YearOfPublish = bookModel.YearOfPublish
            };

            return result;
        }

        public static IEnumerable<PublicationHouseViewModelItem> MapPublicationHouseListToModel(IEnumerable<PublicationHouse> publicationHouseList)
        {
            var result = new List<PublicationHouseViewModelItem>();

            foreach (PublicationHouse publicationHouse in publicationHouseList)
            {
                result.Add(new PublicationHouseViewModelItem
                {
                    Id = publicationHouse.Id,
                    Name = publicationHouse.Name
                });
            }

            return result;
        }

        public static GetSimpleBookViewModel MapBookListToSimpleBookViewModel(IEnumerable<Book> bookList)
        {
            var result = new GetSimpleBookViewModel();
            foreach (var book in bookList)
            {
                result.BookList.Add(new BookGetSimpleBookViewModelItem
                {
                    Id = book.Id,
                    Title = book.Title,
                    AuthorName = book.AuthorName,
                    YearOfPublish = book.YearOfPublish
                });
            }

            return result;
        }

        public static GetComplexBookViewModel MapGetComplexBookViewModel(IEnumerable<Book> bookList, IEnumerable<PublicationHouse> publicationHouseList, IEnumerable<BookPublicationHouse> relationList)
        {
            var result = new GetComplexBookViewModel();

            foreach (var book in bookList)
            {
                var bookViewItem = new BookGetComplexBookViewModelItem()
                {
                    Id = book.Id,
                    Title = book.Title,
                    AuthorName = book.AuthorName,
                    YearOfPublish = book.YearOfPublish
                };

                var tmpRelations = relationList.Where(obj => obj.BookId == book.Id).ToList();

                var publicationHouseIds = tmpRelations.Select(obj => obj.PublicationHouseId);

                var tmpPublicationHouses = publicationHouseList.Where(obj => publicationHouseIds.Contains(obj.Id));

                bookViewItem.PublicationHouseList = MapPublicationHouseListToModel(tmpPublicationHouses);

                result.BookList.Add(bookViewItem);
            }

            return result;
        }
    }
}
