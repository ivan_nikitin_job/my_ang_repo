﻿using AngularLibrary.BusinessLogicLayer.Exceptions.BookExceptions;
using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BLL
{
    public class BookBusinessLogic : IBookBuisnessLogic
    {
        private IBookRepository _booksRepository;

        public BookBusinessLogic(IBookRepository bookRepository)
        {
            _booksRepository = bookRepository;
        }

        public async Task<IEnumerable<Book>> GetBookList()
        {
            IEnumerable<Book> result = await _booksRepository.Get();

            if (result == null)
            {
                throw new BookListErrorException();
            }

            return result;
        }

        public void CreateBook(Book book, IEnumerable<long> publicationHouseIDs)
        {
            bool success = _booksRepository.Create(book, publicationHouseIDs);

            if (!success)
            {
                throw new BookCreateException();
            }
        }

        public void UpdateBook(Book book, IEnumerable<long> publicationHouseIDs)
        {
            bool success = _booksRepository.Update(book, publicationHouseIDs);

            if (!success)
            {
                throw new BookUpdateException();
            }
        }

        public void DeleteBook(long id)
        {
            bool success = _booksRepository.Delete(id);

            if (!success)
            {
                throw new BookDeleteException();
            }
        }
    }
}
