﻿using AngularLibrary.BusinessLogicLayer.Exceptions.BookPublicationHouseExceptions;
using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using System.Collections.Generic;

namespace AngularLibrary.BLL
{
    public class BookPublicationHouseBusinessLogic : IBookPublicationHouseBusinessLogic
    {
        private IBookPublicationHouseRepository _repository;

        public BookPublicationHouseBusinessLogic(IBookPublicationHouseRepository bookPublicationHouseRepository)
        {
            _repository = bookPublicationHouseRepository;
        }

        public IEnumerable<BookPublicationHouse> GetRelations()
        {
            IEnumerable<BookPublicationHouse> result = _repository.Get();

            if (result == null)
            {
                throw new GetRelationsException();
            }

            return result;
        }

        public IEnumerable<BookPublicationHouse> GetRelationsByBookId(long id)
        {
            IEnumerable<BookPublicationHouse> result = _repository.GetByBookID(id);

            if (result == null)
            {
                throw new GetRelationsByBookIDException();
            }

            return result;
        }

        public IEnumerable<BookPublicationHouse> GetRelationsByPublicationHouseId(long id)
        {
            IEnumerable<BookPublicationHouse> result = _repository.GetByPublicationHouseID(id);

            if (result == null)
            {
                throw new GetRelationsByPublicationHouseIDException();
            }

            return result;
        }
    }
}
