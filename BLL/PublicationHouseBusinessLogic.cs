﻿using AngularLibrary.BusinessLogicLayer.Exceptions.PublicationHouseExceptions;
using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BLL
{
    public class PublicationHouseBusinessLogic : IPublicationHouseBusinessLogic
    {
        private IPublicationHouseRepository _publicationHouseRepository;

        public PublicationHouseBusinessLogic(IPublicationHouseRepository publicationHouseRepository)
        {
            _publicationHouseRepository = publicationHouseRepository;
        }

        public async Task<IEnumerable<PublicationHouse>> GetPublicationHouseList()
        {
            IEnumerable<PublicationHouse> result = await _publicationHouseRepository.Get();

            if (result == null)
            {
                throw new GetPublicationHouseListException();
            }

            return result;
        }

        public void CreatePublicationHouse(PublicationHouse publicationHouse, IEnumerable<long> bookIDs)
        {
            bool success = _publicationHouseRepository.Create(publicationHouse, bookIDs);

            if (!success)
            {
                throw new CreatePublicationHouseException();
            }
        }

        public void UpdatePublicationHouse(PublicationHouse publicationHouse, IEnumerable<long> bookIDs)
        {
            bool success = _publicationHouseRepository.Update(publicationHouse, bookIDs);


            if (!success)
            {
                throw new UpdatePublicationHouseException();
            }
        }

        public void DeletePublicationHouse(long id)
        {
            bool success = _publicationHouseRepository.Delete(id);

            if (!success)
            {
                throw new DeletePublicationHouseException();
            }
        }
    }
}
