﻿using AngularLibrary.BusinessLogicLayer.Exceptions.MagazineExceptions;
using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BLL
{
    public class MagazineBusinessLogic : IMagazineBusinessLogic
    {
        private IMagazineRepository _magazineRepository;

        public MagazineBusinessLogic(IMagazineRepository magazineRepository)
        {
            _magazineRepository = magazineRepository;
        }

        public async Task<IEnumerable<Magazine>> GetMagazineList()
        {
            IEnumerable<Magazine> result = await _magazineRepository.Get();

            if (result == null)
            {
                throw new GetMagazineListException();
            }

            return result;
        }

        public void CreateMagazine(Magazine magazine)
        {
            bool success = _magazineRepository.Create(magazine);

            if (!success)
            {
                throw new CreateMagazineException();
            }
        }

        public void UpdateMagazine(Magazine magazine)
        {
            bool success = _magazineRepository.Update(magazine);

            if (!success)
            {
                throw new UpdateMagazineException();
            }
        }

        public void DeleteMagazine(long id)
        {
            bool success = _magazineRepository.Delete(id);

            if (!success)
            {
                throw new DeleteMagazineException();
            }
        }
    }
}
