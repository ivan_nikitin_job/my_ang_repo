﻿using AngularLibrary.BusinessLogicLayer.Exceptions.BrochureExceptions;
using AngularLibrary.BusinessLogicLayer.Interfaces;
using AngularLibrary.DataAccessLayer.Interfaces;
using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BLL
{
    public class BrochureBusinessLogic : IBrochureBusinessLogic
    {
        private IBrochureRepository _repository;

        public BrochureBusinessLogic(IBrochureRepository brochureRepository)
        {
            _repository = brochureRepository;
        }

        public async Task<IEnumerable<Brochure>> GetBrochureList()
        {
            IEnumerable<Brochure> result = await _repository.Get();
            if (result == null)
            {
                throw new GetBrochureListException();
            }

            return result;
        }

        public void CreateBrochure(Brochure brochure)
        {
            bool success = _repository.Create(brochure);

            if (!success)
            {
                throw new CreateBrochureException();
            }
        }

        public void UpdateBrochure(Brochure brochure)
        {
            bool success = _repository.Update(brochure);

            if (!success)
            {
                throw new UpdateBrochureException();
            }
        }

        public void DeleteBrochure(long id)
        {
            bool success = _repository.Delete(id);

            if (!success)
            {
                throw new DeleteBrochureException();
            }
        }
    }
}
