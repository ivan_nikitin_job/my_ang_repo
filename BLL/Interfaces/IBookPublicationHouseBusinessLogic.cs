﻿using AngularLibrary.Entities;
using System.Collections.Generic;

namespace AngularLibrary.BusinessLogicLayer.Interfaces
{
    public interface IBookPublicationHouseBusinessLogic
    {
        IEnumerable<BookPublicationHouse> GetRelations();
        IEnumerable<BookPublicationHouse> GetRelationsByBookId(long id);
        IEnumerable<BookPublicationHouse> GetRelationsByPublicationHouseId(long id);
    }
}
