﻿using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BusinessLogicLayer.Interfaces
{
    public interface IBrochureBusinessLogic
    {
        Task<IEnumerable<Brochure>> GetBrochureList();
        void CreateBrochure(Brochure brochure);
        void UpdateBrochure(Brochure brochure);
        void DeleteBrochure(long id);
    }
}
