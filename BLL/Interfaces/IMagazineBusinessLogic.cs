﻿using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BusinessLogicLayer.Interfaces
{
    public interface IMagazineBusinessLogic
    {
        Task<IEnumerable<Magazine>> GetMagazineList();
        void CreateMagazine(Magazine magazine);
        void UpdateMagazine(Magazine magazine);
        void DeleteMagazine(long id);
    }
}
