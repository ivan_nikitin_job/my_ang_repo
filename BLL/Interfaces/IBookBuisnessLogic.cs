﻿using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BusinessLogicLayer.Interfaces
{
    public interface IBookBuisnessLogic
    {
        Task<IEnumerable<Book>> GetBookList();
        void CreateBook(Book book, IEnumerable<long> relations);
        void UpdateBook(Book book, IEnumerable<long> relations);
        void DeleteBook(long id);
    }
}
