﻿using AngularLibrary.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularLibrary.BusinessLogicLayer.Interfaces
{
    public interface IPublicationHouseBusinessLogic
    {
        Task<IEnumerable<PublicationHouse>> GetPublicationHouseList();
        void CreatePublicationHouse(PublicationHouse publicationHouse, IEnumerable<long> relations);
        void UpdatePublicationHouse(PublicationHouse publicationHouse, IEnumerable<long> relations);
        void DeletePublicationHouse(long id);
    }
}
