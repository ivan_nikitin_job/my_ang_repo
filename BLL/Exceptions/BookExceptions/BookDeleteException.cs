﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BookExceptions
{
    public class BookDeleteException : Exception
    {
        public BookDeleteException()
        {

        }

        public BookDeleteException(string message) : base(message)
        {

        }
    }
}
