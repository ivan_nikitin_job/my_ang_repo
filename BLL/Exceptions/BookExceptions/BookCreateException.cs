﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BookExceptions
{
    public class BookCreateException : Exception
    {
        public BookCreateException()
        {

        }

        public BookCreateException(string message) : base(message)
        {

        }
    }
}
