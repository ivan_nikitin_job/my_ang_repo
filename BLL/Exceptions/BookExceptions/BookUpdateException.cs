﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BookExceptions
{
    public class BookUpdateException : Exception
    {
        public BookUpdateException()
        {

        }

        public BookUpdateException(string message) : base(message)
        {

        }
    }
}
