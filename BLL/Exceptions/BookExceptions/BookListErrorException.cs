﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BookExceptions
{
    public class BookListErrorException : Exception
    {

        public BookListErrorException()
        {

        }

        public BookListErrorException(string message) : base(message)
        {

        }
    }
}
