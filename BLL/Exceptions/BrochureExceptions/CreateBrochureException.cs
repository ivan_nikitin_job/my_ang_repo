﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BrochureExceptions
{
    public class CreateBrochureException : Exception
    {
        public CreateBrochureException()
        {

        }

        public CreateBrochureException(string message) : base(message)
        {

        }
    }
}
