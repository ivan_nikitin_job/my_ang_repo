﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BrochureExceptions
{
    public class GetBrochureListException : Exception
    {
        public GetBrochureListException()
        {

        }

        public GetBrochureListException(string message) : base(message)
        {

        }
    }
}
