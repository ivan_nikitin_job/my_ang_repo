﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BrochureExceptions
{
    public class DeleteBrochureException : Exception
    {
        public DeleteBrochureException()
        {

        }

        public DeleteBrochureException(string message) : base(message)
        {

        }
    }
}
