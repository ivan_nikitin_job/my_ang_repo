﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BrochureExceptions
{
    public class UpdateBrochureException : Exception
    {
        public UpdateBrochureException()
        {

        }

        public UpdateBrochureException(string message) : base(message)
        {

        }
    }
}
