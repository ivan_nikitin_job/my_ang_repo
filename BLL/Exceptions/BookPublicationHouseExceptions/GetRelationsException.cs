﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BookPublicationHouseExceptions
{
    public class GetRelationsException : Exception
    {
        public GetRelationsException()
        {

        }

        public GetRelationsException(string message) : base(message)
        {

        }
    }
}
