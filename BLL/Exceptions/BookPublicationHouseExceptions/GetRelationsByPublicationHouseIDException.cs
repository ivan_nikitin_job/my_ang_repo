﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BookPublicationHouseExceptions
{
    public class GetRelationsByPublicationHouseIDException : Exception
    {
        public GetRelationsByPublicationHouseIDException()
        {

        }

        public GetRelationsByPublicationHouseIDException(string message) : base(message)
        {

        }
    }
}
