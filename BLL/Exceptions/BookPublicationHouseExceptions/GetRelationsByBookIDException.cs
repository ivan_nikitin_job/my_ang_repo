﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.BookPublicationHouseExceptions
{
    public class GetRelationsByBookIDException : Exception
    {
        public GetRelationsByBookIDException()
        {

        }

        public GetRelationsByBookIDException(string message) : base(message)
        {

        }
    }
}
