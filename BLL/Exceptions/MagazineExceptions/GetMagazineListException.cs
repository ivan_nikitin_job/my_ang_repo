﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.MagazineExceptions
{
    public class GetMagazineListException : Exception
    {
        public GetMagazineListException()
        {

        }

        public GetMagazineListException(string message) : base(message)
        {

        }
    }
}
