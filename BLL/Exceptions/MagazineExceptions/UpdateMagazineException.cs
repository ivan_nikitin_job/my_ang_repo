﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.MagazineExceptions
{
    public class UpdateMagazineException : Exception
    {
        public UpdateMagazineException()
        {

        }

        public UpdateMagazineException(string message) : base(message)
        {

        }
    }
}
