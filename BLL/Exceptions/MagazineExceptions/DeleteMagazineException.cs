﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.MagazineExceptions
{
    public class DeleteMagazineException : Exception
    {
        public DeleteMagazineException()
        {

        }

        public DeleteMagazineException(string message) : base(message)
        {

        }
    }
}
