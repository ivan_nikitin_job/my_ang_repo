﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.MagazineExceptions
{
    public class CreateMagazineException : Exception
    {
        public CreateMagazineException()
        {

        }

        public CreateMagazineException(string message) : base(message)
        {

        }
    }
}
