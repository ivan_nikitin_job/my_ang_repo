﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.PublicationHouseExceptions
{
    public class UpdatePublicationHouseException : Exception
    {
        public UpdatePublicationHouseException()
        {

        }

        public UpdatePublicationHouseException(string message) : base(message)
        {

        }
    }
}
