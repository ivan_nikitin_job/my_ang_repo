﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.PublicationHouseExceptions
{
    public class GetPublicationHouseListException : Exception
    {
        public GetPublicationHouseListException()
        {

        }

        public GetPublicationHouseListException(string message) : base(message)
        {

        }
    }
}
