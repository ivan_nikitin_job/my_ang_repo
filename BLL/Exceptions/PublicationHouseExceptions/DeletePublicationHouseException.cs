﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.PublicationHouseExceptions
{
    public class DeletePublicationHouseException : Exception
    {
        public DeletePublicationHouseException()
        {

        }

        public DeletePublicationHouseException(string message) : base(message)
        {

        }
    }
}
