﻿using System;

namespace AngularLibrary.BusinessLogicLayer.Exceptions.PublicationHouseExceptions
{
    public class CreatePublicationHouseException : Exception
    {
        public CreatePublicationHouseException()
        {

        }

        public CreatePublicationHouseException(string message) : base(message)
        {

        }
    }
}
